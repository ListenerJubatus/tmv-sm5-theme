local gc = Var("GameCommand");
return Def.ActorFrame {
	Def.ActorFrame {
		OffCommand=function(self) self:decelerate(0.2):diffusealpha(0):zoom(0.8) end;
		LoadActor(THEME:GetPathG("_style item", "base")) .. {		
			OnCommand=function(self) self:diffusealpha(1) end;
		};
			-- Symbol selector
		Def.Sprite {
			Name="HeaderDiamondIcon",
			InitCommand=function(self) self:horizalign(center):x(0):y(14) end;
			OnCommand=function(self)
				local screen = SCREENMAN:GetTopScreen():GetName()
				if FILEMAN:DoesFileExist(THEME:GetCurrentThemeDirectory() .. "/Graphics/_style icon "..gc:GetText()..".png") then
					self:Load(THEME:GetPathG("_style icon", gc:GetText()))
				else
					print("Style icon error: file does not exist")
					self:Load(THEME:GetPathG("_style icon", "generic"))
				end
			end;
			GainFocusCommand=function(self) 
				self:stoptweening():decelerate(0.1):diffusealpha(1)
			end;
			LoseFocusCommand=function(self) 
				self:stoptweening():decelerate(0.1):diffusealpha(0.5)
			end;
		};
		LoadFont("_risque 48px")..{
			Text=string.upper(gc:GetText());
			InitCommand=function(self)
				self:y(-174):maxwidth(440):zoom(1):horizalign(center):diffuse(color("#434343"))
			end;
		};
		LoadFont("Common Normal")..{
			Text=string.upper(THEME:GetString("StyleType", gc:GetStyle():GetStyleType()));
			InitCommand=function(self)
				self:y(-174+32):maxwidth(440):zoom(1):horizalign(center):diffuse(color("#434343"))
			end;			
		};
	};
};