return Def.ActorFrame {
	LoadActor(THEME:GetPathG("TitleMenu", "item base")) .. {
		InitCommand=function(self) self:diffuse(color("#EDB758")):zoom(0.6) end;
	};	
	LoadActor(THEME:GetPathG("TitleMenu", "item bezel")) .. {
		InitCommand=function(self) self:zoom(0.6):blend('add') end;
		GainFocusCommand=function(self) self:linear(0.1):diffusealpha(0.8) end;
		LoseFocusCommand=function(self) self:linear(0.1):diffusealpha(0.2) end;
	};
	LoadFont("_risque 48px") .. {
		Text=THEME:GetString("TitleMenuItems","Options");
		InitCommand=function(self) self:shadowlength(1):maxwidth(280):zoom(0.75):horizalign(center):diffuse(color("#FFFFFF")):diffusebottomedge(color("#C8D4CA")):strokecolor(color("#324235")):y(-6) end;
	};
};