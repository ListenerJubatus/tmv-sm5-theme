local netConnected = IsNetConnected();
local loggedOnSMO = IsNetSMOnline();

local t = Def.ActorFrame{
	LoadFont("Common Normal") .. {
		InitCommand=function(self) self:zoom(0.5):horizalign(left) end;
		BeginCommand=function(self)
			-- check network status
			if netConnected then
				self:diffuse( color("#237519") );
				self:strokecolor( color("#1EFF00") );
				self:settext( Screen.String("Network OK") );
			else
				self:diffuse( color("1,1,1,1") );
				self:settext( Screen.String("Offline") );
			end;
		end;
	};
};

if netConnected then
	t[#t+1] = LoadFont("Common Normal") .. {
		InitCommand=function(self)
			self:y(16):horizalign(left):zoom(0.5):shadowlength(1):diffuse(color("#237519")):strokecolor(color("#1EFF00"))
		end;
		BeginCommand=function(self)
			self:settext( string.format(Screen.String("Connected to %s"), GetServerName()) );
		end;
	};
end;

return t;