local gauntlet_colors = {
	"#9153E0", -- Hephaestus	1
	"#3CA4E2", -- Euphrosyne	2
	"#28AD91", -- Eir			3
	"#CBC5AE", -- Radha			4
	"#FFAB58", -- Kohara		5
	"#AD000C", -- Hekau			6
	"#0483C7", -- Special for How Far We've Come			7
	"#FFFFFF", -- NaN			8
};

local t = Def.ActorFrame {};
-- Royal Gauntlet gear
if not GAMESTATE:IsCourseMode() then 
		if GAMESTATE:GetCurrentSong():GetDisplayFullTitle() == "Tea & Corruption" then
			t[#t+1] = LoadActor("_sutekh");
		elseif GAMESTATE:GetCurrentSong():GetDisplayFullTitle() == "SELENE" then
			t[#t+1] = LoadActor("_posedion");				
		elseif GAMESTATE:GetCurrentSong():GetDisplayMainTitle() == "Endymion" then
			t[#t+1] = LoadActor("_endymion");		
		elseif GAMESTATE:GetCurrentSong():GetDisplayFullTitle() == "Hearts and Hooves" then
			t[#t+1] = LoadActor("_epona");
		elseif GAMESTATE:GetCurrentSong():GetDisplayFullTitle() == "Séquence de vie" then
			t[#t+1] = LoadActor("_bekmor");
			t[#t+1] = LoadActor("_glowvines") .. {
				OnCommand=function(self)
					self:diffuseramp():effectclock('beat'):effectcolor1(color("1,1,1,0.3")):effectcolor2(color("1,1,1,0.1"))
				end;
			};
		else
		t[#t+1] = LoadActor("_frame") .. {
			OnCommand=function(self) self:playcommand("Set") end;
			CurrentSongChangedMessageCommand=function(self) self:playcommand("Set") end;
			CurrentCourseChangedMessageCommand=function(self) self:playcommand("Set") end;
			CurrentStepsP1ChangedMessageCommand=function(self) self:playcommand("Set") end;
			CurrentStepsP2ChangedMessageCommand=function(self) self:playcommand("Set") end;
			CurrentTrailP1ChangedMessageCommand=function(self) self:playcommand("Set") end;
			CurrentTrailP2ChangedMessageCommand=function(self) self:playcommand("Set") end;
			SetCommand=function(self)
			local curScreen = Var "LoadingScreen";
			local playMode = GAMESTATE:GetPlayMode();
			local curStage = GAMESTATE:GetCurrentStage();
			if GAMESTATE:IsCourseMode() then
					self:diffuse(StageToColor(curStage))
				else
					local song = GAMESTATE:GetCurrentSong():GetDisplayFullTitle()
					if song == "Faster Than a Lightning" then
						self:diffuse(color(gauntlet_colors[1]))
					elseif song == "Art of Manifestation" then
						self:diffuse(color(gauntlet_colors[2]))
					elseif song == "The Outdoors (アイスベア's NATUREMiX)" then
						self:diffuse(color(gauntlet_colors[3]))
					elseif song == "APPLEBUCKIN'" then
						self:diffuse(color(gauntlet_colors[4]))
					elseif song == "Cerebral Meltdown" then
						self:diffuse(color(gauntlet_colors[5]))
					elseif song == "Trixie the Pony Troll (Brilliant Venture & One Track Mind Bootleg Mix)" then
						self:diffuse(color(gauntlet_colors[6]))					
					elseif song == "How Far We've Come" or song == "Got You" then
						self:diffuse(color(gauntlet_colors[7])):diffusetopedge(color("#DC13DC"))
					else
						self:diffuse(StageToColor(curStage))
					end
				end;				
			self:diffusealpha(0.95)
			end;
		};
		
		t[#t+1] = LoadActor(THEME:GetPathG("ScreenGameplay", "top decor"));
	end;
else
	t[#t+1] = LoadActor("_frame") .. {
		OnCommand=function(self) self:playcommand("Set") end;
		CurrentSongChangedMessageCommand=function(self) self:playcommand("Set") end;
		CurrentCourseChangedMessageCommand=function(self) self:playcommand("Set") end;
		CurrentStepsP1ChangedMessageCommand=function(self) self:playcommand("Set") end;
		CurrentStepsP2ChangedMessageCommand=function(self) self:playcommand("Set") end;
		CurrentTrailP1ChangedMessageCommand=function(self) self:playcommand("Set") end;
		CurrentTrailP2ChangedMessageCommand=function(self) self:playcommand("Set") end;
		SetCommand=function(self)
		local curScreen = Var "LoadingScreen";
		local playMode = GAMESTATE:GetPlayMode();
		local curStage = GAMESTATE:GetCurrentStage();
		self:diffuse(color("#6D2308")):diffusetopedge(color("#95391D"))	
		self:diffusealpha(0.95)
		end;
	};
	
	t[#t+1] = LoadActor(THEME:GetPathG("ScreenGameplay", "top decor")) .. {
		OnCommand=function(self)
		self:diffuse(color("#EE5020"))
		end;
	};
end;

return t;