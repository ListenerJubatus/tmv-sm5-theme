return Def.ActorFrame {

	Def.ActorFrame {
		InitCommand=function(self) self:vertalign(top):horizalign(center):y(SCREEN_TOP-84):x(SCREEN_CENTER_X) end;
		BeginCommand=function(self) self:sleep(0.1):decelerate(0.4):y(SCREEN_TOP-2) end;
		OffCommand=function(self) self:decelerate(0.6):addy(-120) end;
		LoadActor("_middle") .. {
			InitCommand=function(self) self:vertalign(top):horizalign(center) end;
		};
		LoadActor("_base") .. {
			InitCommand=function(self) self:vertalign(top):horizalign(center) end;
		};
		LoadActor("_metalshine") .. {
			InitCommand=function(self) self:vertalign(top):horizalign(center):addy(57):zoomto(SCREEN_WIDTH,6):blend('add') end;
			OnCommand=function(self) self:customtexturerect(0,0,1,1):texcoordvelocity(0.05,0) end;
		};
		LoadActor("_metalshine") .. {
			InitCommand=function(self) self:vertalign(top):horizalign(center):addy(1):zoomto(SCREEN_WIDTH,4):blend('add') end;
			OnCommand=function(self) self:customtexturerect(0,0,1,1):texcoordvelocity(-0.03,0) end;
		};
		Def.Sprite {
			Name="HeaderBadge",
			InitCommand=function(self) self:horizalign(center):zoom(0.35):addy(32) end;
			OnCommand=function(self)
				local screen = SCREENMAN:GetTopScreen():GetName()
				if FILEMAN:DoesFileExist("Themes/"..THEME:GetCurThemeName().."/Graphics/ScreenWithMenuElements header/"..screen.." badge.png") then
					self:Load(THEME:GetPathG("","ScreenWithMenuElements header/"..screen.." badge"))
				-- Little workaround so not every other options menu has the "graphic missing" icon.
				elseif string.find(screen, "Options") then
					self:Load(THEME:GetPathG("","ScreenWithMenuElements header/_options badge"))
				else
					print("iconerror: file does not exist")
					self:Load(THEME:GetPathG("","ScreenWithMenuElements header/_generic badge"))
				end
			end;
			OffCommand=function(self) self:decelerate(0.4):diffusealpha(0):zoom(0.15) end;
		};
	};

	-- Text

	LoadFont("ScreenWithMenuElements header") .. {
		Name="HeaderText";
		Text=Screen.String("HeaderText");
		InitCommand=function(self)
			self:xy(SCREEN_LEFT+60,SCREEN_TOP+28):horizalign(left):maxwidth(440):zoom(0.6)
			self:diffuse(color("#FFFFFF"))
			self:strokecolor(color("#000000"))
		end;
		BeginCommand=function(self) self:diffusealpha(0):zoomx(0.5):sleep(0.4):decelerate(0.4):diffusealpha(1):zoomx(0.6) end;
		OffCommand=function(self) self:decelerate(0.5):addy(-120) end;
		UpdateScreenHeaderMessageCommand=function(self,param)
			self:settext(param.Header);
		end;
	};

};