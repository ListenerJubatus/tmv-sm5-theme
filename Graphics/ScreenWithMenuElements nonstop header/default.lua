return Def.ActorFrame {

	Def.ActorFrame {
		InitCommand=function(self) self:vertalign(top):horizalign(center):y(SCREEN_TOP-84):x(SCREEN_CENTER_X) end;
		BeginCommand=function(self) self:sleep(0.1):decelerate(0.4):y(SCREEN_TOP-2) end;
		OffCommand=function(self) self:decelerate(0.6):addy(-120) end;
		LoadActor("_base") .. {
			InitCommand=function(self) self:vertalign(top):horizalign(center) end;
		};
	};

	LoadFont("ScreenWithMenuElements header") .. {
		Name="HeaderText";
		Text=Screen.String("HeaderText");
		InitCommand=function(self)
			self:xy(SCREEN_LEFT+72,SCREEN_TOP+28):horizalign(left):maxwidth(440):zoom(0.6)
			self:diffuse(color("#E99E3C"))
			self:strokecolor(color("#9D2A22"))
		end;
		BeginCommand=function(self) self:diffusealpha(0):zoomx(0.5):sleep(0.4):decelerate(0.4):diffusealpha(1):zoomx(0.6) end;
		OffCommand=function(self) self:decelerate(0.5):addy(-120) end;
		UpdateScreenHeaderMessageCommand=function(self,param)
			self:settext(param.Header);
		end;
	};	

};