return Def.ActorFrame {
	Def.Quad {
		InitCommand=function(self) self:diffuse(color("#000000")):zoomto(54,54):diffusealpha(0.6) end;
	};
	Def.Banner{
		InitCommand=function(self) self:scaletoclipped(52,52) end;
			SetMessageCommand=function(self,params)
			if params.Text then
				self:LoadFromSongGroup(params.Text);
			else
				-- fallback
				self:Load( THEME:GetPathG("Group jacket","fallback") );
			end;
		end;
	};
	--- Mini jackets
	Def.Sprite {
		SetCommand=function(self,params)
			local group = params.Text;
			if group == "TrotMania V" then
				self:Load( THEME:GetPathG("_minijacket","tm5") );
			elseif group == "TrotMania IV" then
				self:Load( THEME:GetPathG("_minijacket","tm4") );
			elseif group == "TrotMania III" then
				self:Load( THEME:GetPathG("_minijacket","tm3") );
			elseif group == "TrotMania Chrystalize" then
				self:Load( THEME:GetPathG("_minijacket","tm2") );
			elseif group == "TrotMania" then
				self:Load( THEME:GetPathG("_minijacket","tm1") );
			elseif group == "Trials of the Embers" then
				self:Load( THEME:GetPathG("_minijacket","tmtrials") );
			elseif group == "In the Groove 3 OG" then
				self:Load( THEME:GetPathG("_minijacket","itg3") );
			elseif group == "In the Groove 2" then
				self:Load( THEME:GetPathG("_minijacket","itg2") );
			elseif group == "In the Groove" then
				self:Load( THEME:GetPathG("_minijacket","tm1") );			
			elseif group == "DanceDanceRevolution A" then
				self:Load( THEME:GetPathG("_minijacket","ddra") );			
			elseif group == "DanceDanceRevolution A20" then
				self:Load( THEME:GetPathG("_minijacket","ddr20th") );			
			elseif group == "UKSRT8" then
				self:Load( THEME:GetPathG("_minijacket","uksrt8") );
			else
				self:Load( THEME:GetPathG("_minijacket","fallback") );
			end;
		end;
	};
};