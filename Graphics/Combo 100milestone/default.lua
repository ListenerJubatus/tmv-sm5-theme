return Def.ActorFrame {
	LoadActor("_keybox") .. {
		InitCommand=function(self)
			self:diffusealpha(0):blend('add'):addy(25) 
		end;
		MilestoneCommand=function(self)
			self:rotationz(0):zoom(0.2):diffusealpha(0.8):smooth(0.4):zoom(0.75):diffusealpha(0):rotationz(29)
		end;
	};
};