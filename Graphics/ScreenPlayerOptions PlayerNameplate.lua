local PlayerNumber = ...
assert( PlayerNumber )

local bpm_text_zoom = 0.75

local song_bpms= {}
local bpm_text= "??? - ???"
local function format_bpm(bpm)
	return ("%.0f"):format(bpm)
end

-- Courses don't have GetDisplayBpms.
if GAMESTATE:GetCurrentSong() then
	song_bpms= GAMESTATE:GetCurrentSong():GetDisplayBpms()
	song_bpms[1]= math.round(song_bpms[1])
	song_bpms[2]= math.round(song_bpms[2])
	if song_bpms[1] == song_bpms[2] then
		bpm_text= format_bpm(song_bpms[1])
	else
		bpm_text= format_bpm(song_bpms[1]) .. " - " .. format_bpm(song_bpms[2])
	end
end

return Def.ActorFrame {
	LoadActor( THEME:GetPathG("ScreenHeader", "menu flag") ) .. {
		InitCommand=function(self) self:diffusealpha(1) end;
		BeginCommand=function(self)
			self:playcommand("Set")
		end;		
		SetCommand=function(self)
			self:diffuse(ColorDarkTone(PlayerColor(PlayerNumber)))
		end;
	};
	LoadActor( THEME:GetPathG("ScreenHeader", "flag overlay") ) .. {
	};	
	
	
	LoadFont("_amaranth 24px") .. {
		Text=ToEnumShortString(PlayerNumber);
		Name="PlayerShortName",
		InitCommand=function(self) self:maxwidth(120):horizalign(center):zoom(0.5):y(-8) end;
		OnCommand= function(self) self:diffuse(ColorLightTone(PlayerColor(PlayerNumber))) end;
	},
	LoadFont("_amaranth Bold 24px") .. {
		Text="100 - 200000";
		Name="BPMRangeNew",
		InitCommand= function(self)
			self:x(0):y(6):horizalign(center):maxwidth(88/bpm_text_zoom):zoom(bpm_text_zoom)
			local speed, mode= GetSpeedModeAndValueFromPoptions(PlayerNumber)
			self:playcommand("SpeedChoiceChanged", {pn= PlayerNumber, mode= mode, speed= speed})
		end,
		BPMWillNotChangeCommand=function(self) self:stopeffect() end;
		BPMWillChangeCommand=function(self) self:diffuseshift():effectcolor1(Color.White):effectcolor2(0.949, 0.831, 0.631, 1) end;
		SpeedChoiceChangedMessageCommand= function(self, param)
			if param.pn ~= PlayerNumber then return end
			local text= ""
			local no_change= true
			if param.mode == "x" then
				if not song_bpms[1] then
					text= "??? - ???"
				elseif song_bpms[1] == song_bpms[2] then
					text= format_bpm(song_bpms[1] * param.speed*.01)
				else
					text= format_bpm(song_bpms[1] * param.speed*.01) .. " - " ..
						format_bpm(song_bpms[2] * param.speed*.01)
				end
				no_change= param.speed == 100
			elseif param.mode == "C" then
				text= param.mode .. param.speed
				no_change= param.speed == song_bpms[2] and song_bpms[1] == song_bpms[2]
			else
				no_change= param.speed == song_bpms[2]
				if song_bpms[1] == song_bpms[2] then
					text= param.mode .. param.speed
				else
					local factor= song_bpms[1] / song_bpms[2]
					text= param.mode .. format_bpm(param.speed * factor) .. " - "
						.. param.mode .. param.speed
				end
			end
			self:settext(text)
			if no_change then
				self:queuecommand("BPMWillNotChange")
			else
				self:queuecommand("BPMWillChange")
			end
		end
	}
}