return Def.ActorFrame {
	SetCommand=function(self)
		self:GetParent():GetChild("SectionCount"):visible(false)
	end;
	Def.Sprite {
		SetCommand=function(self)
			if not GAMESTATE:IsCourseMode() then
				self:Load( THEME:GetPathG("","MusicWheelItem SectionCollapsed NormalPart/_base") );
			else
				self:Load( THEME:GetPathG("","MusicWheelItem SectionCollapsed NormalPart/_course") );
			end;
		end;
	};
	
	Def.Sprite {
		SetCommand=function(self,params)
			if not GAMESTATE:IsCourseMode() then
				local group = params.Text;
				if group == "TrotMania V" then
					self:Load( THEME:GetPathG("SectionCollapsed","tm5") );
				elseif group == "TrotMania IV" then
					self:Load( THEME:GetPathG("SectionCollapsed","tm4") );
				elseif group == "TrotMania III" then
					self:Load( THEME:GetPathG("SectionCollapsed","tm3") );
				elseif group == "TrotMania Chrystalize" then
					self:Load( THEME:GetPathG("SectionCollapsed","tm2") );
				elseif group == "TrotMania" then
					self:Load( THEME:GetPathG("SectionCollapsed","tm1") );
				elseif group == "Trials of the Embers" then
					self:Load( THEME:GetPathG("SectionCollapsed","tmtrials") );			
				elseif group == "TM Mod Lab" then
					self:Load( THEME:GetPathG("SectionCollapsed","modlab") );
				elseif group == "UKSRT8" then
					self:Load( THEME:GetPathG("SectionCollapsed","uksrt8") );
				else
					self:Load( THEME:GetPathG("","_blank") );			
				end;
			else
				self:Load( THEME:GetPathG("","_blank") );			
			end;
		end;
	};
	-- Thumbnail
	LoadActor(THEME:GetPathG("MusicWheelItem", "SectionThumb")) .. {
		InitCommand=function(self) self:x(-169):visible(not GAMESTATE:IsCourseMode()) end;
	};
};