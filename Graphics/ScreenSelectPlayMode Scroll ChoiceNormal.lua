return Def.ActorFrame {
	LoadActor( THEME:GetPathG("PlayModeOption", "normal") ) .. {
		OnCommand=function(self) self:zoom(1) end;
		OffCommand=function(self) self:sleep(0.1):smooth(0.2):zoom(1.2):diffusealpha(0) end;
	};	
	LoadActor( THEME:GetPathG("PlayModeOption", "underlay") ) .. {
		OnCommand=function(self) self:zoom(1):y(270) end;
		GainFocusCommand=function(self) 
			self:stoptweening():decelerate(0.2):diffusealpha(1)
		end;
		LoseFocusCommand=function(self) 
			self:stoptweening():decelerate(0.1):diffusealpha(0)
		end;
	};
	LoadFont("Common Large")..{
		Text=THEME:GetString("ScreenSelectPlayMode", "StandardExplanation");
		InitCommand=function(self) self:y(270):zoom(0.75):horizalign(center):diffuse(color("#E5C551")):diffusetopedge(color("#ECDEAB")):strokecolor(Color.Black):shadowlength(1):maxwidth(620/0.75) end;
		GainFocusCommand=function(self) 
			self:stoptweening():decelerate(0.2):diffusealpha(1)
		end;
		LoseFocusCommand=function(self) 
			self:stoptweening():decelerate(0.1):diffusealpha(0)
		end;
	};	
};