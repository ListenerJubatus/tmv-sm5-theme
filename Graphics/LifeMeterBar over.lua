if GAMESTATE:IsCourseMode() then
	return LoadActor(THEME:GetPathG("ScreenGameplay", "course frame") );
else
	if GAMESTATE:GetCurrentSong():GetDisplayFullTitle() == "Tea & Corruption" then
		return LoadActor(THEME:GetPathG("LifebarFrame", "sutekh") );
	elseif GAMESTATE:GetCurrentSong():GetDisplayFullTitle() == "SELENE" then
		return LoadActor(THEME:GetPathG("LifebarFrame", "posedion") );
	elseif GAMESTATE:GetCurrentSong():GetDisplayMainTitle() == "Endymion" then
		return LoadActor(THEME:GetPathG("LifebarFrame", "aces") );
	elseif GAMESTATE:GetCurrentSong():GetDisplayFullTitle() == "Hearts and Hooves" then
		return LoadActor(THEME:GetPathG("LifebarFrame", "epona") );
	elseif GAMESTATE:GetCurrentSong():GetDisplayFullTitle() == "Séquence de vie" then
		return LoadActor(THEME:GetPathG("LifebarFrame", "hrimfaxi") );
	else
		return LoadActor(THEME:GetPathG("ScreenGameplay", "life frame") );
	end
end;