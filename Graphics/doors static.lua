return Def.ActorFrame {
	LoadActor(THEME:GetPathG("_door", "bottom")) .. {
		InitCommand=function(self)
			self:horizalign(center):vertalign(top):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y-8)
		end;
	};	
	LoadActor(THEME:GetPathG("_door", "top")) .. {
		InitCommand=function(self)
			self:horizalign(center):vertalign(bottom):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y+100-10)
		end;
	};	
};