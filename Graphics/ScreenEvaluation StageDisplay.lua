local curScreen = Var "LoadingScreen";
local curStageIndex = GAMESTATE:GetCurrentStageIndex();
return Def.ActorFrame {
	LoadActor( THEME:GetPathG("ScreenHeader", "menu flag") ) .. {
		InitCommand=function(self) self:diffusealpha(1) end;
		BeginCommand=function(self)
			local top = SCREENMAN:GetTopScreen()
			if top then
				if not string.find(top:GetName(),"ScreenEvaluation") then
					curStageIndex = curStageIndex + 1
				end
			end
			self:playcommand("Set")
		end;
		CurrentSongChangedMessageCommand=function(self) self:playcommand("Set") end;
		SetCommand=function(self)
			local curStage = GAMESTATE:GetCurrentStage();
			self:diffuse(StageToColor(curStage));
		end;
	};
	LoadActor( THEME:GetPathG("ScreenHeader", "flag overlay") ) .. {
	};	
	
	LoadFont("_decadentafrax 24px") .. {
		InitCommand=function(self) self:y(5):zoom(1):horizalign(center):shadowlength(1):maxwidth(100) end;
		BeginCommand=function(self)
			local top = SCREENMAN:GetTopScreen()
			if top then
				if not string.find(top:GetName(),"ScreenEvaluation") then
					curStageIndex = curStageIndex + 1
				end
			end
			self:playcommand("Set")
		end;
		CurrentSongChangedMessageCommand=function(self) self:playcommand("Set") end;
		SetCommand=function(self)
			local curStage = GAMESTATE:GetCurrentStage();
			if GAMESTATE:GetCurrentCourse() then
				self:settext( curStageIndex+1 .. " / " .. GAMESTATE:GetCurrentCourse():GetEstimatedNumStages() );
			elseif GAMESTATE:IsEventMode() then
				self:settextf("Free");
			else
				local thed_stage= thified_curstage_index(curScreen:find("Evaluation"))
				if THEME:GetMetric(curScreen,"StageDisplayUseShortString") then
					self:settext(thed_stage)
					self:zoom(1);
				else
					self:settextf("%s Stage", thed_stage);
					self:zoom(1);
				end;
			end;
			self:strokecolor(color("#000000")):diffuse(color("#FFFFFF"));
		end;
	};
};