local transform = function(self,offsetFromCenter,itemIndex,numitems)
	self:y( offsetFromCenter * 23 );
	if offsetFromCenter < 0 or offsetFromCenter > 5 then
		self:diffusealpha(0)
	else
		self:diffusealpha(1)
	end
end
return Def.CourseContentsList {
	MaxSongs = 10;
    NumItemsToDraw = 8;
	ShowCommand=function(self) self:bouncebegin(0.3):zoomy(1) end;
	HideCommand=function(self) self:linear(0.3):diffusealpha(0) end;
	SetCommand=function(self)
		self:SetFromGameState();
		self:SetCurrentAndDestinationItem(0);
		self:SetPauseCountdownSeconds(1);
		self:SetSecondsPauseBetweenItems( 0.5 );
		self:SetTransformFromFunction(transform);
		
		self:SetDestinationItem( math.max(0,self:GetNumItems() - 4) );
		self:SetLoop(false);
		self:SetMask(0,0);
	end;
	CurrentTrailP1ChangedMessageCommand=function(self) self:playcommand("Set") end;
	CurrentTrailP2ChangedMessageCommand=function(self) self:playcommand("Set") end;

	Display = Def.ActorFrame { 
		InitCommand=function(self) self:setsize(270,44) end;

		LoadActor(THEME:GetPathG("CourseEntryDisplay","bar")) .. {
			SetSongCommand=function(self, params)
				if params.Difficulty then
-- 					self:diffuse( SONGMAN:GetSongColor(params.Song) );
					self:diffuse( CustomDifficultyToColor(params.Difficulty) );
				else
					self:diffuse( color("#FFFFFF") );
-- 					self:diffuse( CustomDifficultyToColor(params.Difficulty) );
				end
					self:finishtweening():diffusealpha(0):sleep(0.125*params.Number):linear(0.125):diffusealpha(1)
					:linear(0.05):glow(color("1,1,1,0.5")):decelerate(0.1):glow(color("1,1,1,0"))
			end;
		};

		Def.TextBanner {
			InitCommand=function(self) self:x(-128):y(0):zoom(1):Load("TextBannerCourse"):SetFromString("", "", "", "", "", "") end;
			SetSongCommand=function(self, params)
				if params.Song then
					if GAMESTATE:GetCurrentCourse():GetDisplayFullTitle() == "Abomination" then
						-- abomination hack
						if PREFSMAN:GetPreference("EasterEggs") then
							if params.Number % 2 ~= 0 then
								-- turkey march
								local artist = params.Song:GetDisplayArtist();
								self:SetFromString( "Turkey", "", "", "", artist, "" );
							else
								self:SetFromSong( params.Song );
							end;
						else
							self:SetFromSong( params.Song );
						end;
					else
						self:SetFromSong( params.Song );
					end;
					self:diffuse( CustomDifficultyToLightColor(params.Difficulty) );
-- 					self:glow("1,1,1,0.5");
				else
					self:SetFromString( "??????????", "??????????", "", "", "", "" );
					self:diffuse( color("#FFFFFF") );
-- 					self:glow("1,1,1,0");
				end
				self:finishtweening():diffusealpha(0):sleep(0.125*params.Number):smooth(0.3):diffusealpha(1)				
			end;
		};

 		LoadFont("Common Normal") .. {
			Text="0";
			InitCommand=function(self) self:x(100):y(-6):zoom(0.75):shadowlength(1) end;
			SetSongCommand=function(self, params)
				if params.PlayerNumber ~= GAMESTATE:GetMasterPlayerNumber() then return end
				self:settext( params.Meter );
				self:diffuse( CustomDifficultyToColor(params.Difficulty) );
				self:finishtweening():diffusealpha(0):sleep(0.125*params.Number):smooth(0.3):diffusealpha(1)
			end;
		}; 

	};
};