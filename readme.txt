h1. Bonjour

TrotMania V StepMania theme by Lirodon & co.
http://trotmania.ponyvillefm.com/

h1. Notes
* As always, 16:9 mode only. Works best with Event Mode.
* Tested and developed primarily on StepMania 5.1 Beta 2, should work on 5.0.12 too.

h1. Third-party content credits

h2. Art
* Mid tier BG #1: _magic_art_x [ https://www.instagram.com/_magic_art_x/ ]
* Mid tier BG #4: Lirodon respect for Diskette and Hungarian folklore
* Mid tier BG #5: moo [ https://twitter.com/ConstantMelody / https://twitter.com/jaezmien ]
* Top tier BG #1, #4, and 6: Artwork by LimeArtsu [ https://www.deviantart.com/limeartsu/ ]
* Top tier BG #5: Artwork by honeyntoast [ https://www.deviantart.com/honeyntoast/ ]
* Top tier BG #7: Artwork by smulesscootaloo523 [ https://www.deviantart.com/smulesscootaloo523 ]

h2. Sounds

* https://freesound.org/people/clruwe/sounds/119178/
* https://freesound.org/people/cstaier/sounds/361857/
* https://freesound.org/people/volivieri/sounds/110012/ [used in ScreenTitleMenu music]

See readme files in song folders for art credits relating to individual songs.