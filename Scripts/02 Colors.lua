GameColor = {
	PlayerColors = {
		PLAYER_1	= color("#60adff"),
		PLAYER_2	= color("#F6C659"),
	},
	PlayerDarkColors = {
		PLAYER_1	= color("#3c73ad"),
		PLAYER_2	= color("#E9B132"),
	},
	Difficulty = {
		--[[ These are for 'Custom' Difficulty Ranks. It can be very  useful
		in some cases, especially to apply new colors for stuff you
		couldn't before. (huh? -aj) ]]
		Beginner	= color("#E46FD5"),			-- Yearling
		Easy		= color("#008DE1"),			-- Crusader
		Medium		= color("#16B098"),			-- Protege
		Hard		= color("#F1AA59"),			-- Mane-iac
		Challenge	= color("#DF5C13"),			-- Nightmare
		Edit		= color("#88AEA5"),	-- gray
		Couple		= color("#ed0972"),			-- hot pink
		Routine		= color("#ff9a00"),			-- orange
		--[[ These are for courses, so let's slap them here in case someone
		wanted to use Difficulty in Course and Step regions. ]]
		Difficulty_Beginner	= color("#E46FD5"),		-- purple
		Difficulty_Easy		= color("#008DE1"),		-- green
		Difficulty_Medium	= color("#16B098"),		-- yellow
		Difficulty_Hard		= color("#F1AA59"),		-- red
		Difficulty_Challenge	= color("#DF5C13"),	-- light blue
		Difficulty_Edit 	= color("#88AEA5"),		-- gray
		Difficulty_Couple	= color("#ed0972"),				-- hot pink
		Difficulty_Routine	= color("#ff9a00")				-- orange
	},
	Stage = {
		Stage_1st	= color("#824194"),
		Stage_2nd	= color("#824194"),
		Stage_3rd	= color("#824194"),
		Stage_4th	= color("#824194"),
		Stage_5th	= color("#824194"),
		Stage_6th	= color("#824194"),
		Stage_Next	= color("#824194"),
		Stage_Final	= color("#A21B89"),
		Stage_Extra1	= color("#14DF8D"),
		Stage_Extra2	= color("#F89E41"),
		Stage_Nonstop	= color("#911B0B"),
		Stage_Oni	= color("#BC4E12"),
		Stage_Endless	= color("#E99E3C"),
		Stage_Event	= color("#824194"),
		Stage_Demo	= color("#824194")
	},
	Judgment = {
		JudgmentLine_W1		= color("#FAB7CE"),
		JudgmentLine_W2		= color("#A3E4E8"),
		JudgmentLine_W3		= color("#D1EA80"),
		JudgmentLine_W4		= color("#e8ded7"),
		JudgmentLine_W5		= color("#f9c8b1"),
		JudgmentLine_Held	= color("#FFFFFF"),
		JudgmentLine_Miss	= color("#A35B5B"),
		JudgmentLine_MaxCombo	= color("#A2CFDD")
	},
};

GameColor.Difficulty["Crazy"] = GameColor.Difficulty["Hard"];
GameColor.Difficulty["Freestyle"] = GameColor.Difficulty["Easy"];
GameColor.Difficulty["Nightmare"] = GameColor.Difficulty["Challenge"];
GameColor.Difficulty["HalfDouble"] = GameColor.Difficulty["Medium"];