local music_wheel_item_text_part_names= {
	"Song",
	"SectionExpanded",
	"SectionCollapsed",
	"Roulette",
	"Course",
	"Sort",
	"Mode",
	"Random",
	"Portal",
	"Custom",
};

function hide_unwanted_music_wheel_text(item)
	for i, name in pairs(music_wheel_item_text_part_names) do
		local part= item:GetChild(name)
		if part then part:visible(false) end
	end
end