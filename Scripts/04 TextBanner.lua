function ArtistSetConversion(self)
	local Title=self:GetChild("Title");
	local Subtitle=self:GetChild("Subtitle");
	local Artist=self:GetChild("Artist");
	if Subtitle:GetText() == "" then
		Title:zoom(1):maxwidth(290):xy(-185,-9):diffuse(color("#33293E"))
		Subtitle:visible(false)
		Artist:zoom(0.5):maxwidth(290/0.5):xy(-185,12):diffuse(color("#33293E"))
	else
		Title:zoom(0.8):maxwidth(290/0.8):xy(-185,-14):diffuse(color("#33293E"))
		Subtitle:visible(true):zoom(0.5):maxwidth(290/0.5):xy(-185,4):diffuse(color("#33293E"))
		Artist:zoom(0.5):maxwidth(290/0.5):xy(-185,16):diffuse(color("#33293E"))
	end
end

function CourseSetConversion(self)
	local Title=self:GetChild("Title");
	local Subtitle=self:GetChild("Subtitle");
	local Artist=self:GetChild("Artist");
	if Subtitle:GetText() == "" then
		Title:zoom(0.65):maxwidth(160/0.5):y(-6)
		Subtitle:visible(false)
	else
		Title:zoom(0.5):maxwidth(160/0.5):y(-10)
		Subtitle:visible(true):zoom(0.4):y(2):maxwidth(160/0.4)
	end
end
collectgarbage();