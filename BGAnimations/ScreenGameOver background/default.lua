return Def.ActorFrame {

	Def.Quad { -- BG
		InitCommand=function(self)
			self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):Center():diffuse(color("#000000"))
		end;
	};
		
	LoadActor("_go") .. {
		InitCommand=function(self)
			self:vertalign(bottom):x(SCREEN_CENTER_X):y(SCREEN_TOP)
		end;
		OnCommand=function(self)
			self:decelerate(2):y(SCREEN_CENTER_Y+170)
		end;
	};
		
	LoadActor("_shutdown") .. {
		OnCommand=function(self)
			self:queuecommand("Play")
		end;
		PlayCommand=function(self)
			self:play()
		end;
	};

};