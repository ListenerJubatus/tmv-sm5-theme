local t = Def.ActorFrame {};

local playMode = GAMESTATE:GetPlayMode()

local sStage = ""
sStage = GAMESTATE:GetCurrentStage()

if playMode ~= 'PlayMode_Regular' and playMode ~= 'PlayMode_Rave' and playMode ~= 'PlayMode_Battle' then
  sStage = playMode;
end;

if not (GAMESTATE:IsCourseMode() or GAMESTATE:IsExtraStage() or GAMESTATE:IsExtraStage2()) then
        local tRemap = {
                Stage_Event             = 0,
                Stage_1st               = 1,
                Stage_2nd               = 2,
                Stage_3rd               = 3,
                Stage_4th               = 4,
                Stage_5th               = 5,
                Stage_6th               = 6,
                Stage_Final               = 7,
                Stage_Extra1               = 8,
                Stage_Extra2              = 9,
        };

        local nSongCount = tRemap[sStage] + (GAMESTATE:GetCurrentSong():GetStageCost()-1);

        if nSongCount >= PREFSMAN:GetPreference("SongsPerPlay") then
        sStage = "Stage_Final";
        else
                sStage = sStage;
        end;
end;

local t = Def.ActorFrame {};

t[#t+1] = Def.ActorFrame {
	Def.Quad {
		InitCommand=function(self) self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y) end;
		OnCommand=function(self) self:diffuse(color("#FFFFFF")) end;
		};
	LoadActor("_pat") .. {
			InitCommand=function(self)
				self:rotationy(0):rotationz(15):zoomto(SCREEN_WIDTH*2,SCREEN_HEIGHT*2):customtexturerect(0,0,SCREEN_WIDTH*4/1024,SCREEN_HEIGHT*4/1024)
				self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y) 
			end;
			OnCommand=function(self) self:texcoordvelocity(0.075,0.075):diffuse(color("#396B16")) end;
	};
	LoadActor("_sun") .. {
	InitCommand=function(self) self:Center():diffusealpha(0.5) end;
	OnCommand=function(self) self:spin():effectmagnitude(0,0,3) end;
	};

	LoadActor( THEME:GetPathG("ScreenStageInformation", "Stage " .. ToEnumShortString(sStage) ) ) .. {
		InitCommand=function(self) self:Center() end;	
		OnCommand=function(self)
			self:rotationz(5):zoom(0.4):diffusealpha(0):bouncebegin(0.7):rotationz(0)
			self:diffusealpha(1):zoom(.9):bounceend(0.4):zoom(0.8):sleep(0.6+2.7)
			self:decelerate(0.5):zoom(0.4):diffusealpha(0)
		end;
	};
};

	-- BG bar for song title
	t[#t+1] = LoadActor(THEME:GetPathG("ScreenStageInformation", "Normal frame")) .. {
		InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_BOTTOM+49):zoomy(-1) end;
		OnCommand=function(self) 
			self:playcommand("Set"):draworder(129):sleep(0.7):decelerate(0.3):addy(-80)
			self:bouncebegin(0.2):addy(3):bounceend(0.3):addy(-3):sleep(3):decelerate(0.5):addy(80)
		end;			
		SetCommand=function(self)
			local curScreen = Var "LoadingScreen";
			local playMode = GAMESTATE:GetPlayMode();
			local curStage = GAMESTATE:GetCurrentStage();
			self:diffuse(StageToColor(curStage));
		end;
	};

	-- Step author credits
	if GAMESTATE:IsHumanPlayer(PLAYER_1) == true then
	t[#t+1] = Def.ActorFrame {
	InitCommand=function(self) self:y(SCREEN_BOTTOM-48):x(SCREEN_LEFT+20) end;
	OnCommand=function(self) self:diffusealpha(0):sleep(1.5):smooth(0.4):diffusealpha(1):sleep(2.6):decelerate(0.5):addy(80) end;
		LoadFont("Common Medium") .. {
		  OnCommand=function(self) self:playcommand("Set"):horizalign(left):zoom(0.75) end;
          SetCommand=function(self)
				local stepsP1 = GAMESTATE:GetCurrentSteps(PLAYER_1)
				local song = GAMESTATE:GetCurrentSong();
				if song then 
					if stepsP1 ~= nil then
						local st = stepsP1:GetStepsType();
						local diff = stepsP1:GetDifficulty();
						local courseType = GAMESTATE:IsCourseMode() and SongOrCourse:GetCourseType() or nil;
						local cd = GetCustomDifficulty(st, diff, courseType);
							if stepsP1:IsAnEdit() then
								self:settext(stepsP1:GetChartName())
							else
								self:settext(THEME:GetString("CustomDifficulty",ToEnumShortString(diff)));
							end;
						self:diffuse(ColorLightTone(CustomDifficultyToColor(cd)));
						self:strokecolor(ColorDarkTone(CustomDifficultyToColor(cd)));
					else
						self:settext("")
					end
				else
					self:settext("")
				end
         end;
		};
		LoadFont("Common Normal") .. {
		  InitCommand=function(self) self:addy(15) end;
		  OnCommand=function(self) self:playcommand("Set"):horizalign(left):zoom(0.5):diffuse(color("#FFFFFF")):strokecolor(color("#000000")):maxwidth(300) end;
          SetCommand=function(self)
			local stepsP1 = GAMESTATE:GetCurrentSteps(PLAYER_1)
			local song = GAMESTATE:GetCurrentSong();
			if song then
				if stepsP1 ~= nil then
					self:settext(stepsP1:GetAuthorCredit())
				else
					self:settext("")
				end
			else
				self:settext("")
			end
         end
		};
	};
	end
	
	if GAMESTATE:IsHumanPlayer(PLAYER_2) == true then
	t[#t+1] = Def.ActorFrame {
	InitCommand=function(self) self:y(SCREEN_BOTTOM-48):x(SCREEN_RIGHT-20) end;
	OnCommand=function(self) self:diffusealpha(0):sleep(1.5):smooth(0.4):diffusealpha(1):sleep(2.6):decelerate(0.5):addy(80) end;
	LoadFont("Common Medium") .. {
		  OnCommand=function(self) self:playcommand("Set"):horizalign(right):zoom(0.75) end;
		  SetCommand=function(self)
				local stepsP2 = GAMESTATE:GetCurrentSteps(PLAYER_2)
				local song = GAMESTATE:GetCurrentSong();
				if song then 
					if stepsP2 ~= nil then
						local st = stepsP2:GetStepsType();
						local diff = stepsP2:GetDifficulty();
						local courseType = GAMESTATE:IsCourseMode() and SongOrCourse:GetCourseType() or nil;
						local cd = GetCustomDifficulty(st, diff, courseType);
							if stepsP2:IsAnEdit() then
								self:settext(stepsP2:GetChartName())
							else
								self:settext(THEME:GetString("CustomDifficulty",ToEnumShortString(diff)));
							end;
						self:diffuse(ColorLightTone(CustomDifficultyToColor(cd)));
						self:strokecolor(ColorDarkTone(CustomDifficultyToColor(cd)));
					else
						self:settext("")
					end
				else
					self:settext("")
				end
         end;
		};

	LoadFont("Common Normal") .. {
		  InitCommand=function(self) self:addy(15) end;
		  OnCommand=function(self) self:playcommand("Set"):horizalign(right):zoom(0.5):diffuse(color("#FFFFFF")):strokecolor(color("#000000")):maxwidth(300) end;
          SetCommand=function(self)
			local stepsP2 = GAMESTATE:GetCurrentSteps(PLAYER_2)
			local song = GAMESTATE:GetCurrentSong();
			if song then
				if stepsP2 ~= nil then
					self:settext(stepsP2:GetAuthorCredit())
				else
					self:settext("")
				end
			else
				self:settext("")
			end
          end
	};
	};
	end

t[#t+1] = Def.ActorFrame {
	InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_BOTTOM-35) end;
	OnCommand=function(self) self:diffusealpha(0):sleep(1.5):smooth(0.4):diffusealpha(1):sleep(2.6):decelerate(0.5):addy(80) end;
	LoadFont("Common Normal") .. {
		Text=GAMESTATE:IsCourseMode() and GAMESTATE:GetCurrentCourse():GetDisplayFullTitle() or GAMESTATE:GetCurrentSong():GetTranslitFullTitle();
		InitCommand=function(self) self:diffuse(color("#FFFFFF")):strokecolor(color("#000000")):maxwidth(SCREEN_WIDTH*0.75) end;
		OnCommand=function(self) self:zoom(0.75) end;
	};
	LoadFont("Common Normal") .. {
		Text=GAMESTATE:IsCourseMode() and ToEnumShortString( GAMESTATE:GetCurrentCourse():GetCourseType() ) or GAMESTATE:GetCurrentSong():GetDisplayArtist();
		InitCommand=function(self) self:diffuse(color("#FFFFFF")):strokecolor(color("#000000")):maxwidth(SCREEN_WIDTH*0.75) end;
		OnCommand=function(self) self:zoom(0.6):addy(20) end;
	};
};

t[#t+1] = Def.Quad {
    InitCommand=function(self)
		self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):diffuse(color("#FFFFFF")) 
	end;
    OnCommand=function(self)
		self:diffusealpha(1):sleep(0.1):decelerate(0.8):diffusealpha(0)
	end;
};  
t[#t+1] = Def.Quad {
	InitCommand=function(self)
		self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):diffuse(color("#000000")) 
	end;
    OnCommand=function(self)
		self:diffusealpha(0):sleep(5):linear(0.6):diffusealpha(1)
	end;
};


t[#t+1] = LoadActor("_sound.ogg") .. {
        OnCommand=function(self) self:queuecommand("Play") end;
		PlayCommand=function(self) self:play() end;
    };

return t;
