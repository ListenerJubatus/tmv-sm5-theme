if not GAMESTATE:IsCourseMode() then
	-- branch from the start
	local song = GAMESTATE:GetCurrentSong():GetDisplayFullTitle()
	if song == "Faster Than a Lightning" then
		return LoadActor( THEME:GetPathB("","ScreenStageInformation underlay/_specialone"))
	elseif song == "Art of Manifestation" then
		return LoadActor( THEME:GetPathB("","ScreenStageInformation underlay/_specialone"))
	elseif song == "The Outdoors (アイスベア's NATUREMiX)" then
		return LoadActor( THEME:GetPathB("","ScreenStageInformation underlay/_specialone"))
	elseif song == "APPLEBUCKIN'" then
		return LoadActor( THEME:GetPathB("","ScreenStageInformation underlay/_specialone"))
	elseif song == "Cerebral Meltdown" then
		return LoadActor( THEME:GetPathB("","ScreenStageInformation underlay/_specialone"))
	elseif song == "Trixie the Pony Troll (Brilliant Venture & One Track Mind Bootleg Mix)" then
		return LoadActor( THEME:GetPathB("","ScreenStageInformation underlay/_specialone"))	
	elseif song == "Séquence de vie" then
		return LoadActor( THEME:GetPathB("","ScreenStageInformation underlay/_royal"))	
	elseif song == "SELENE" then
		return LoadActor( THEME:GetPathB("","ScreenStageInformation underlay/_royal"))	
	elseif song == "Tea & Corruption" then
		return LoadActor( THEME:GetPathB("","ScreenStageInformation underlay/_royal"))	
	elseif song == "Hearts and Hooves" then
		return LoadActor( THEME:GetPathB("","ScreenStageInformation underlay/_royal"))
	else
		return LoadActor( THEME:GetPathB("","ScreenStageInformation underlay/_normal"))
	end;
else
	return LoadActor( THEME:GetPathB("","ScreenStageInformation underlay/_course"))
end