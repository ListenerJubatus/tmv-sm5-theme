local t = Def.ActorFrame {};

local playMode = GAMESTATE:GetPlayMode()

local sStage = ""
sStage = GAMESTATE:GetCurrentStage()

if playMode ~= 'PlayMode_Regular' and playMode ~= 'PlayMode_Rave' and playMode ~= 'PlayMode_Battle' then
  sStage = playMode;
end;

if not (GAMESTATE:IsCourseMode() or GAMESTATE:IsExtraStage() or GAMESTATE:IsExtraStage2()) then
        local tRemap = {
                Stage_Event             = 0,
                Stage_1st               = 1,
                Stage_2nd               = 2,
                Stage_3rd               = 3,
                Stage_4th               = 4,
                Stage_5th               = 5,
                Stage_6th               = 6,
                Stage_Final               = 7,
                Stage_Extra1               = 8,
                Stage_Extra2              = 9,
        };

        local nSongCount = tRemap[sStage] + (GAMESTATE:GetCurrentSong():GetStageCost()-1);

        if nSongCount >= PREFSMAN:GetPreference("SongsPerPlay") then
        sStage = "Stage_Final";
        else
                sStage = sStage;
        end;
end;

local gauntlet_colors = {
	"#9153E0", -- Hephaestus	1
	"#3CA4E2", -- Euphrosyne	2
	"#28AD91", -- Eir			3
	"#CBC5AE", -- Radha			4
	"#FFAB58", -- Kohara		5
	"#AD000C", -- Hekau			6
	"#FF0000", -- NaN			7
};

local picked_song = 99
local function gauntlet_song_color()
	if picked_song == 99 then
		local song = GAMESTATE:GetCurrentSong():GetDisplayFullTitle()
		if song == "Faster Than a Lightning" then
			picked_song = 1
		elseif song == "Art of Manifestation" then
			picked_song = 2
		elseif song == "The Outdoors (アイスベア's NATUREMiX)" then
			picked_song = 3
		elseif song == "APPLEBUCKIN'" then
			picked_song = 4
		elseif song == "Cerebral Meltdown" then
			picked_song = 5
		elseif song == "Trixie the Pony Troll (Brilliant Venture & One Track Mind Bootleg Mix)" then
			picked_song = 6
		else
			picked_song = 7
		end
	end;
	return gauntlet_colors[picked_song]
end;

local t = Def.ActorFrame {};

t[#t+1] = Def.Quad {
	InitCommand=function(self) self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y) end;
	OnCommand=function(self) self:diffuse(color("#000000")) end;
	};
	
t[#t+1] = LoadActor("_light") .. {
	InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):zoomto(SCREEN_WIDTH,SCREEN_HEIGHT) end;
	OnCommand=function(self)
		self:diffuse(color( gauntlet_song_color() )):diffusealpha(0)
		self:sleep(0.8):diffusealpha(1):decelerate(3):diffusealpha(0.25)
	end;
	};

t[#t+1] = LoadActor("_pat") .. {
	InitCommand=function(self)
		self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):customtexturerect(0,0,SCREEN_WIDTH*4/256,SCREEN_HEIGHT*4/256):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y)
	end;
	OnCommand=function(self)
		self:texcoordvelocity(0,2.1):diffusealpha(0.02)
	end;
	};		
	
t[#t+1] = LoadActor("crack") .. {
	InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):zoomto(SCREEN_WIDTH,SCREEN_HEIGHT) end;
	OnCommand=function(self)
		self:diffuse(color( gauntlet_song_color() )):diffusealpha(0)
		self:sleep(0.8):diffusealpha(1):decelerate(0.6):diffusealpha(0):zoomx(1.2)
	end;
	};	
		
t[#t+1] = LoadActor("_emblem") .. {
	InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):diffusealpha(0):zoom(0.5) end;
	OnCommand=function(self)
		self:diffuse(color( gauntlet_song_color()) ):diffusealpha(0)
		self:decelerate(0.5):zoom(0.75):diffusealpha(1)
	end;
	};			
	
t[#t+1] = LoadActor("_blade") .. {
	InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):addx(900):addy(900):zoom(0.72) end;
	OnCommand=function(self)
		self:sleep(0.3):decelerate(0.5):addx(-900):addy(-900)
	end;
	};
	
t[#t+1] = LoadActor("_blade") .. {
	InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):addx(-900):addy(900):zoom(0.72):zoomx(-0.72) end;
	OnCommand=function(self)
		self:sleep(0.3):decelerate(0.5):addx(900):addy(-900)
	end;
	};	

t[#t+1] = LoadActor("_shineblade") .. {
	InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):blend('add'):zoom(0.72) end;
	OnCommand=function(self)
		self:diffuse( ColorLightTone(color( gauntlet_song_color() )) ):diffusealpha(0)
		self:sleep(0.8):diffusealpha(0.3):decelerate(2):zoom(2):diffusealpha(0)
	end;
	};

t[#t+1] = LoadActor("_title") .. {
	InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):diffusealpha(0):zoom(0.5) end;
	OnCommand=function(self)
		self:diffusealpha(0):sleep(0.8):decelerate(2):zoom(1):diffusealpha(1)
	end;
	};				

	-- BG bar for song title
	t[#t+1] = LoadActor(THEME:GetPathG("ScreenStageInformation", "Gauntlet frame")) .. {
		InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_BOTTOM+49) end;
		OnCommand=function(self) 
			self:diffuse(ColorMidTone (color( gauntlet_song_color() )) ):draworder(129):sleep(0.7):decelerate(0.3):addy(-80)
			self:bouncebegin(0.2):addy(3):bounceend(0.3):addy(-3):sleep(3):decelerate(0.5):addy(80)
		end;
	};
	
	-- Step author credits
	if GAMESTATE:IsHumanPlayer(PLAYER_1) == true then
	t[#t+1] = Def.ActorFrame {
	InitCommand=function(self) self:y(SCREEN_BOTTOM-48):x(SCREEN_LEFT+20) end;
	OnCommand=function(self) self:diffusealpha(0):sleep(1.5):smooth(0.4):diffusealpha(1):sleep(2.6):decelerate(0.5):addy(80) end;
		LoadFont("Common Medium") .. {
		  OnCommand=function(self) self:playcommand("Set"):horizalign(left):zoom(0.75) end;
          SetCommand=function(self)
				local stepsP1 = GAMESTATE:GetCurrentSteps(PLAYER_1)
				local song = GAMESTATE:GetCurrentSong();
				if song then 
					if stepsP1 ~= nil then
						local st = stepsP1:GetStepsType();
						local diff = stepsP1:GetDifficulty();
						local courseType = GAMESTATE:IsCourseMode() and SongOrCourse:GetCourseType() or nil;
						local cd = GetCustomDifficulty(st, diff, courseType);
							if stepsP1:IsAnEdit() then
								self:settext(stepsP1:GetChartName())
							else
								self:settext(THEME:GetString("CustomDifficulty",ToEnumShortString(diff)));
							end;
						self:diffuse(ColorLightTone(CustomDifficultyToColor(cd)));
						self:strokecolor(ColorDarkTone(CustomDifficultyToColor(cd)));
					else
						self:settext("")
					end
				else
					self:settext("")
				end
         end;
		};
		LoadFont("Common Normal") .. {
		  InitCommand=function(self) self:addy(15) end;
		  OnCommand=function(self) self:playcommand("Set"):horizalign(left):zoom(0.5):diffuse(color("#FFFFFF")):strokecolor(color("#000000")):maxwidth(300) end;
          SetCommand=function(self)
			local stepsP1 = GAMESTATE:GetCurrentSteps(PLAYER_1)
			local song = GAMESTATE:GetCurrentSong();
			if song then
				if stepsP1 ~= nil then
					self:settext(stepsP1:GetAuthorCredit())
				else
					self:settext("")
				end
			else
				self:settext("")
			end
         end
		};
	};
	end
	
	if GAMESTATE:IsHumanPlayer(PLAYER_2) == true then
	t[#t+1] = Def.ActorFrame {
	InitCommand=function(self) self:y(SCREEN_BOTTOM-48):x(SCREEN_RIGHT-20) end;
	OnCommand=function(self) self:diffusealpha(0):sleep(1.5):smooth(0.4):diffusealpha(1):sleep(2.6):decelerate(0.5):addy(80) end;
	LoadFont("Common Medium") .. {
		  OnCommand=function(self) self:playcommand("Set"):horizalign(right):zoom(0.75) end;
		  SetCommand=function(self)
				local stepsP2 = GAMESTATE:GetCurrentSteps(PLAYER_2)
				local song = GAMESTATE:GetCurrentSong();
				if song then 
					if stepsP2 ~= nil then
						local st = stepsP2:GetStepsType();
						local diff = stepsP2:GetDifficulty();
						local courseType = GAMESTATE:IsCourseMode() and SongOrCourse:GetCourseType() or nil;
						local cd = GetCustomDifficulty(st, diff, courseType);
							if stepsP2:IsAnEdit() then
								self:settext(stepsP2:GetChartName())
							else
								self:settext(THEME:GetString("CustomDifficulty",ToEnumShortString(diff)));
							end;
						self:diffuse(ColorLightTone(CustomDifficultyToColor(cd)));
						self:strokecolor(ColorDarkTone(CustomDifficultyToColor(cd)));
					else
						self:settext("")
					end
				else
					self:settext("")
				end
         end;
		};

	LoadFont("Common Normal") .. {
		  InitCommand=function(self) self:addy(15) end;
		  OnCommand=function(self) self:playcommand("Set"):horizalign(right):zoom(0.5):diffuse(color("#FFFFFF")):strokecolor(color("#000000")):maxwidth(300) end;
          SetCommand=function(self)
			local stepsP2 = GAMESTATE:GetCurrentSteps(PLAYER_2)
			local song = GAMESTATE:GetCurrentSong();
			if song then
				if stepsP2 ~= nil then
					self:settext(stepsP2:GetAuthorCredit())
				else
					self:settext("")
				end
			else
				self:settext("")
			end
          end
	};
	};
	end

	t[#t+1] = Def.ActorFrame {
		InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_BOTTOM-50) end;
		OnCommand=function(self) self:diffusealpha(0):sleep(1.5):smooth(0.4):diffusealpha(1):sleep(2.6):decelerate(0.5):addy(80) end;
		LoadFont("Common Normal") .. {
			Text=GAMESTATE:IsCourseMode() and GAMESTATE:GetCurrentCourse():GetDisplayFullTitle() or GAMESTATE:GetCurrentSong():GetTranslitFullTitle();
			InitCommand=function(self) self:diffuse(color("#FFFFFF")):strokecolor(color("#000000")):maxwidth(SCREEN_WIDTH*0.75) end;
			OnCommand=function(self) self:zoom(0.75) end;
		};
		LoadFont("Common Normal") .. {
			Text=GAMESTATE:IsCourseMode() and ToEnumShortString( GAMESTATE:GetCurrentCourse():GetCourseType() ) or GAMESTATE:GetCurrentSong():GetDisplayArtist();
			InitCommand=function(self) self:diffuse(color("#FFFFFF")):strokecolor(color("#000000")):maxwidth(SCREEN_WIDTH*0.75) end;
			OnCommand=function(self) self:zoom(0.6):addy(20) end;
		};
	};


t[#t+1] = Def.Quad {
    InitCommand=function(self)
		self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):diffuse(color("#FFFFFF")) 
	end;
    OnCommand=function(self)
		self:diffusealpha(1):sleep(0.1):decelerate(0.8):diffusealpha(0)
	end;
};  
t[#t+1] = Def.Quad {
	InitCommand=function(self)
		self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):diffuse(color("#000000")) 
	end;
    OnCommand=function(self)
		self:diffusealpha(0):sleep(5):linear(0.6):diffusealpha(1)
	end;
};


t[#t+1] = LoadActor("_sound.ogg") .. {
	OnCommand=function(self) self:queuecommand("Play") end;
	PlayCommand=function(self) self:play() end;
};

return t;