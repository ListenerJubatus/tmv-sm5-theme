local t = Def.ActorFrame {};

local playMode = GAMESTATE:GetPlayMode()

local sStage = ""
sStage = GAMESTATE:GetCurrentStage()

if playMode ~= 'PlayMode_Regular' and playMode ~= 'PlayMode_Rave' and playMode ~= 'PlayMode_Battle' then
  sStage = playMode;
end;

if not (GAMESTATE:IsCourseMode() or GAMESTATE:IsExtraStage() or GAMESTATE:IsExtraStage2()) then
        local tRemap = {
                Stage_Event             = 0,
                Stage_1st               = 1,
                Stage_2nd               = 2,
                Stage_3rd               = 3,
                Stage_4th               = 4,
                Stage_5th               = 5,
                Stage_6th               = 6,
                Stage_Final               = 7,
                Stage_Extra1               = 8,
                Stage_Extra2              = 9,
        };

        local nSongCount = tRemap[sStage] + (GAMESTATE:GetCurrentSong():GetStageCost()-1);

        if nSongCount >= PREFSMAN:GetPreference("SongsPerPlay") then
        sStage = "Stage_Final";
        else
                sStage = sStage;
        end;
end;

local t = Def.ActorFrame {};

t[#t+1] = Def.ActorFrame {
	Def.ActorFrame {
		LoadActor("_underlay") .. {
			InitCommand=function(self) self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y) end;
			OnCommand=function(self)
				self:zoom(1.3):smooth(4):zoom(1)
			end;
		};
		LoadActor("_dia") .. {
			InitCommand=function(self)
				self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):customtexturerect(0,0,SCREEN_WIDTH*4/256,SCREEN_HEIGHT*4/256):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y)
			end;
			OnCommand=function(self)
				self:texcoordvelocity(0.06,0.06):diffusealpha(0.04)
			end;
		};			
		LoadActor("_rails") .. {
			InitCommand=function(self)
				self:zoomto(SCREEN_WIDTH+100,SCREEN_HEIGHT):customtexturerect(0,0,2,1):x(SCREEN_CENTER_X+8):y(SCREEN_CENTER_Y):rotationz(14)
			end;
			OnCommand=function(self)
				self:texcoordvelocity(2.5,0):diffusealpha(0.6)
				self:smooth(4):rotationz(-45)
			end;
		};

		Def.ActorFrame {
			InitCommand=function(self) self:Center() end;
			LoadActor( THEME:GetPathG("ScreenStageInformation", "Stage " .. ToEnumShortString(sStage) ) ) .. {
				OnCommand=function(self)
					self:diffusealpha(0):sleep(0.3):decelerate(2):zoom(1):diffusealpha(1)
				end;
			};
		};
	};
};

	-- BG bar for song title
	t[#t+1] = LoadActor(THEME:GetPathG("ScreenStageInformation", "Normal frame")) .. {
		InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_BOTTOM+49):zoomy(-1) end;
		OnCommand=function(self) 
			self:diffuse(color("#911B0B")):draworder(129):sleep(0.7):decelerate(0.3):addy(-80)
			self:bouncebegin(0.2):addy(3):bounceend(0.3):addy(-3):sleep(3):decelerate(0.5):addy(80)
		end;
	};

	t[#t+1] = Def.ActorFrame {
		InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_BOTTOM-35) end;
		OnCommand=function(self) self:diffusealpha(0):sleep(1.5):smooth(0.4):diffusealpha(1):sleep(2.6):decelerate(0.5):addy(80) end;
		LoadFont("Common Normal") .. {
			Text=GAMESTATE:IsCourseMode() and GAMESTATE:GetCurrentCourse():GetDisplayFullTitle() or GAMESTATE:GetCurrentSong():GetTranslitFullTitle();
			InitCommand=function(self) self:diffuse(color("#FFFFFF")):strokecolor(color("#000000")):maxwidth(SCREEN_WIDTH*0.75) end;
			OnCommand=function(self) self:zoom(0.75) end;
		};
		LoadFont("Common Normal") .. {
			Text=GAMESTATE:IsCourseMode() and ToEnumShortString( GAMESTATE:GetCurrentCourse():GetCourseType() ) or GAMESTATE:GetCurrentSong():GetDisplayArtist();
			InitCommand=function(self) self:diffuse(color("#FFFFFF")):strokecolor(color("#000000")):maxwidth(SCREEN_WIDTH*0.75) end;
			OnCommand=function(self) self:zoom(0.6):addy(20) end;
		};
	};


t[#t+1] = Def.Quad {
    InitCommand=function(self)
		self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):diffuse(color("#FFFFFF")) 
	end;
    OnCommand=function(self)
		self:diffusealpha(1):sleep(0.1):decelerate(0.8):diffusealpha(0)
	end;
};  
t[#t+1] = Def.Quad {
	InitCommand=function(self)
		self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):diffuse(color("#000000")) 
	end;
    OnCommand=function(self)
		self:diffusealpha(0):sleep(5):linear(0.6):diffusealpha(1)
	end;
};


t[#t+1] = LoadActor("_sound.ogg") .. {
        OnCommand=function(self) self:queuecommand("Play") end;
		PlayCommand=function(self) self:play() end;
    };

return t;
