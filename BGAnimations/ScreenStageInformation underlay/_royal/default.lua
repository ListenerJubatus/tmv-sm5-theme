local t = Def.ActorFrame {};

local playMode = GAMESTATE:GetPlayMode()

local sStage = ""
sStage = GAMESTATE:GetCurrentStage()

if playMode ~= 'PlayMode_Regular' and playMode ~= 'PlayMode_Rave' and playMode ~= 'PlayMode_Battle' then
  sStage = playMode;
end;

if not (GAMESTATE:IsCourseMode() or GAMESTATE:IsExtraStage() or GAMESTATE:IsExtraStage2()) then
        local tRemap = {
                Stage_Event             = 0,
                Stage_1st               = 1,
                Stage_2nd               = 2,
                Stage_3rd               = 3,
                Stage_4th               = 4,
                Stage_5th               = 5,
                Stage_6th               = 6,
                Stage_Final               = 7,
                Stage_Extra1               = 8,
                Stage_Extra2              = 9,
        };

        local nSongCount = tRemap[sStage] + (GAMESTATE:GetCurrentSong():GetStageCost()-1);

        if nSongCount >= PREFSMAN:GetPreference("SongsPerPlay") then
        sStage = "Stage_Final";
        else
                sStage = sStage;
        end;
end;

local picked_song = 99
local function gauntlet_stage_id()
	if picked_song == 99 then
		local song = GAMESTATE:GetCurrentSong():GetDisplayFullTitle()
		if song == "Séquence de vie" then
			picked_song = 1
		elseif song == "Tea & Corruption" then
			picked_song = 2
		elseif song == "SELENE" then
			picked_song = 3
		elseif song == "Hearts and Hooves" then
			picked_song = 4
		else
			picked_song = 5
		end
	end;
	return picked_song 
end;

local gauntlet_colors = {
	"#0D17C8", --	1
	"#1AA771", -- 	2
	"#095EA8", --	3
	"#1AA771", -- 	4
	"#666666", -- 	5
};

local t = Def.ActorFrame {};

t[#t+1] = Def.Quad {
	InitCommand=function(self) self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y) end;
	OnCommand=function(self) self:diffuse(color("#000000")) end;
	};
	
t[#t+1] = LoadActor("_bg.jpg") .. {
	InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y) end;
	OnCommand=function(self)
		self:zoom(1.2):decelerate(1.76):zoom(0.92)
	end;
	};
	
t[#t+1] = LoadActor("_sparks") .. {
	InitCommand=function(self)
		self:Center():blend('add'):diffusealpha(0.6)
	end;
	OnCommand=function(self)
		self:spin():effectmagnitude(0,0,4)
	end;
};

t[#t+1] = Def.Quad {
	InitCommand=function(self) self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y) end;
	OnCommand=function(self) 
		self:diffuse(color(gauntlet_colors[gauntlet_stage_id()])):blend('add')
		self:diffusealpha(0):sleep(1.5):linear(4):diffusealpha(0.5)
	end;
	};

t[#t+1] = LoadActor("_shields/" .. gauntlet_stage_id()) .. {
	InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y) end;
	OnCommand=function(self)
		self:zoom(0.4):diffusealpha(0):sleep(2):rotationz(20):decelerate(1.3):zoom(0.6):diffusealpha(1):rotationz(0)
	end;
};


t[#t+1] = LoadActor("_title") .. {
	InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):diffusealpha(0):zoom(1):blend('add') end;
	OnCommand=function(self)
		self:diffusealpha(0):addx(-900):decelerate(0.6):addx(900):diffusealpha(0.4):sleep(0.1):diffusealpha(0)
	end;
	};
	
t[#t+1] = LoadActor("_title") .. {
	InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):diffusealpha(0):zoom(1) end;
	OnCommand=function(self)
		self:diffusealpha(0):sleep(0.6):diffusealpha(0.4):linear(0.2):diffusealpha(1):zoom(1.1):decelerate(1):zoom(1)
	end;
	};	
	
t[#t+1] = LoadActor("_title") .. {
	InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):diffusealpha(0):zoom(1):blend('add') end;
	OnCommand=function(self)
		self:diffusealpha(0):addx(900):decelerate(0.6):addx(-900):diffusealpha(0.4):smooth(2):diffusealpha(0):zoom(2)
	end;
	};

	-- BG bar for song title
	t[#t+1] = LoadActor("_gate/" .. gauntlet_stage_id()) .. {
		InitCommand=function(self) self:vertalign(bottom):x(SCREEN_CENTER_X):y(SCREEN_BOTTOM):zoomy(-1) end;
		OnCommand=function(self)
			self:decelerate(3):addy(-240)
		end;
	};	
	
	t[#t+1] = LoadActor("_gate/" .. gauntlet_stage_id()) .. {
		InitCommand=function(self) self:vertalign(bottom):x(SCREEN_CENTER_X):y(SCREEN_TOP) end;
		OnCommand=function(self)
			self:decelerate(3):addy(240)
		end;
	};
	
	-- Step author credits
	if GAMESTATE:IsHumanPlayer(PLAYER_1) == true then
	t[#t+1] = Def.ActorFrame {
	InitCommand=function(self) self:y(SCREEN_BOTTOM-48):x(SCREEN_LEFT+20) end;
	OnCommand=function(self) self:diffusealpha(0):sleep(1.5):smooth(0.4):diffusealpha(1):sleep(2.6) end;
		LoadFont("Common Medium") .. {
		  OnCommand=function(self) self:playcommand("Set"):horizalign(left):zoom(0.75) end;
          SetCommand=function(self)
				local stepsP1 = GAMESTATE:GetCurrentSteps(PLAYER_1)
				local song = GAMESTATE:GetCurrentSong();
				if song then 
					if stepsP1 ~= nil then
						local st = stepsP1:GetStepsType();
						local diff = stepsP1:GetDifficulty();
						local courseType = GAMESTATE:IsCourseMode() and SongOrCourse:GetCourseType() or nil;
						local cd = GetCustomDifficulty(st, diff, courseType);
							if stepsP1:IsAnEdit() then
								self:settext(stepsP1:GetChartName())
							else
								self:settext(THEME:GetString("CustomDifficulty",ToEnumShortString(diff)));
							end;
						self:diffuse(ColorLightTone(CustomDifficultyToColor(cd)));
						self:strokecolor(ColorDarkTone(CustomDifficultyToColor(cd)));
					else
						self:settext("")
					end
				else
					self:settext("")
				end
         end;
		};
		LoadFont("Common Normal") .. {
		  InitCommand=function(self) self:addy(15) end;
		  OnCommand=function(self) self:playcommand("Set"):horizalign(left):zoom(0.5):diffuse(color("#FFFFFF")):strokecolor(color("#000000")):maxwidth(300) end;
          SetCommand=function(self)
			local stepsP1 = GAMESTATE:GetCurrentSteps(PLAYER_1)
			local song = GAMESTATE:GetCurrentSong();
			if song then
				if stepsP1 ~= nil then
					self:settext(stepsP1:GetAuthorCredit())
				else
					self:settext("")
				end
			else
				self:settext("")
			end
         end
		};
	};
	end
	
	if GAMESTATE:IsHumanPlayer(PLAYER_2) == true then
	t[#t+1] = Def.ActorFrame {
	InitCommand=function(self) self:y(SCREEN_BOTTOM-48):x(SCREEN_RIGHT-20) end;
	OnCommand=function(self) self:diffusealpha(0):sleep(1.5):smooth(0.4):diffusealpha(1) end;
	LoadFont("Common Medium") .. {
		  OnCommand=function(self) self:playcommand("Set"):horizalign(right):zoom(0.75) end;
		  SetCommand=function(self)
				local stepsP2 = GAMESTATE:GetCurrentSteps(PLAYER_2)
				local song = GAMESTATE:GetCurrentSong();
				if song then 
					if stepsP2 ~= nil then
						local st = stepsP2:GetStepsType();
						local diff = stepsP2:GetDifficulty();
						local courseType = GAMESTATE:IsCourseMode() and SongOrCourse:GetCourseType() or nil;
						local cd = GetCustomDifficulty(st, diff, courseType);
							if stepsP2:IsAnEdit() then
								self:settext(stepsP2:GetChartName())
							else
								self:settext(THEME:GetString("CustomDifficulty",ToEnumShortString(diff)));
							end;
						self:diffuse(ColorLightTone(CustomDifficultyToColor(cd)));
						self:strokecolor(ColorDarkTone(CustomDifficultyToColor(cd)));
					else
						self:settext("")
					end
				else
					self:settext("")
				end
         end;
		};

	LoadFont("Common Normal") .. {
		  InitCommand=function(self) self:addy(15) end;
		  OnCommand=function(self) self:playcommand("Set"):horizalign(right):zoom(0.5):diffuse(color("#FFFFFF")):strokecolor(color("#000000")):maxwidth(300) end;
          SetCommand=function(self)
			local stepsP2 = GAMESTATE:GetCurrentSteps(PLAYER_2)
			local song = GAMESTATE:GetCurrentSong();
			if song then
				if stepsP2 ~= nil then
					self:settext(stepsP2:GetAuthorCredit())
				else
					self:settext("")
				end
			else
				self:settext("")
			end
          end
	};
	};
	end

	t[#t+1] = Def.ActorFrame {
		InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_BOTTOM-50) end;
		OnCommand=function(self) self:diffusealpha(0):sleep(1.5):smooth(0.4):diffusealpha(1):sleep(2.6) end;
		LoadFont("Common Normal") .. {
			Text=GAMESTATE:IsCourseMode() and GAMESTATE:GetCurrentCourse():GetDisplayFullTitle() or GAMESTATE:GetCurrentSong():GetTranslitFullTitle();
			InitCommand=function(self) 	self:diffuse(color("#C7894C")):diffusebottomedge(color("#ECDEAB")):strokecolor(Color.Black):maxwidth(SCREEN_WIDTH*0.75) end;
			OnCommand=function(self) self:zoom(0.75) end;
		};
		LoadFont("Common Normal") .. {
			Text=GAMESTATE:IsCourseMode() and ToEnumShortString( GAMESTATE:GetCurrentCourse():GetCourseType() ) or GAMESTATE:GetCurrentSong():GetDisplayArtist();
			InitCommand=function(self) 	self:diffuse(color("#C7894C")):diffusebottomedge(color("#F9D189")):strokecolor(Color.Black):maxwidth(SCREEN_WIDTH*0.75) end;
			OnCommand=function(self) self:zoom(0.6):addy(20) end;
		};
	};


t[#t+1] = Def.Quad {
    InitCommand=function(self)
		self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):diffuse(color("#F2CB90")) 
	end;
    OnCommand=function(self)
		self:diffusealpha(1):sleep(0.1):decelerate(0.8):diffusealpha(0)
	end;
};  
t[#t+1] = Def.Quad {
	InitCommand=function(self)
		self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):diffuse(color("#000000")) 
	end;
    OnCommand=function(self)
		self:diffusealpha(0):sleep(5):linear(1.2):diffusealpha(1)
	end;
};

t[#t+1] = LoadActor("_sound.ogg") .. {
	OnCommand=function(self) self:queuecommand("Play") end;
	PlayCommand=function(self) self:play() end;
};

	t[#t+1] = LoadActor("_fanfare/" .. gauntlet_stage_id()) .. {
	OnCommand=function(self) self:sleep(1.2):queuecommand("Play") end;
	PlayCommand=function(self) self:play() end;
};

return t;