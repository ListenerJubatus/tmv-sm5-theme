return Def.ActorFrame {
	Def.Quad {
		InitCommand=function(self) self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):Center():diffuse(color("#C3F0F3")):diffusebottomedge(color("#17FFD1")) end;
	};
	
	LoadActor("_dia") .. {
	InitCommand=function(self)
		self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):customtexturerect(0,0,SCREEN_WIDTH*4/256,SCREEN_HEIGHT*4/256):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y)
	end;
	OnCommand=function(self)
		self:texcoordvelocity(-0.5,0.25)
		self:diffusealpha(0):decelerate(2):diffusealpha(0.04)
	end;
	};

	Def.ActorFrame {
		InitCommand=function(self) self:Center():zoom(0.7) end;
		OnCommand=function(self) self:addy(300):decelerate(2):addy(-300) end;
			LoadActor("_bg") .. {
			};		
			LoadActor("_fg") .. {
			};
	};
	
	LoadActor("_railing") .. {
		InitCommand=function(self) 
			self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y+30):zoom(0.7)
		end;
		OnCommand=function(self) self:addy(220):decelerate(2):addy(-220)
		end;
	};
};