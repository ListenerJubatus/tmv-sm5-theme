return Def.ActorFrame {
	Def.Quad {
		InitCommand=function(self) self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):Center():diffuse(color("#C3F0F3")):diffusebottomedge(color("#17FFD1")) end;
	};
	LoadActor("_pat") .. {
		InitCommand=function(self)
			self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):customtexturerect(0,0,SCREEN_WIDTH*4/256,SCREEN_HEIGHT*4/256):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y)
		end;
		OnCommand=function(self) self:texcoordvelocity(0.07,0.07):diffusealpha(0.02) end;
	};	
	Def.ActorFrame {
		InitCommand=function(self)
			self:diffusealpha(1):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y+80)
		end,				
		Def.Sprite {
			Name="flagA",
			Texture="flag 9x1.png",
			InitCommand=function(self)
				self:SetAllStateDelays(0.077)
			end,
			OnCommand=function(self)
				self:zoom(0.5):x(-150):y(-160)
			end
		};		
		Def.Sprite {
			Name="flagB",
			Texture="flag 9x1.png",
			InitCommand=function(self)
				self:SetAllStateDelays(0.077)
			end,
			OnCommand=function(self)
				self:zoom(0.35):x(302):y(-98)
			end
		};
		Def.Sprite {
			Name="towers",
			Texture="towers.png",
			InitCommand=function(self)
				self:zoom(0.8):diffusealpha(1)
			end
		};
	};
};