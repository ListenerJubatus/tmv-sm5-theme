return Def.ActorFrame {
	Def.Quad {
		InitCommand=function(self)
			self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):diffuse(color("#838383")):diffusetopedge(color("#4F4F4F")) :Center()
		end;
	};	
	
	LoadActor (GetSongBackground()) .. {
		InitCommand=function(self)
			self:scaletoclipped(SCREEN_WIDTH,SCREEN_HEIGHT):diffusealpha(0.1):Center()
		end;
	};	
	
	LoadActor(THEME:GetPathG("_bg", "diapattern")) .. {
		InitCommand=function(self)
			self:scaletoclipped(SCREEN_WIDTH,SCREEN_HEIGHT):diffusealpha(0.8):Center()
		end;	
	};
};
