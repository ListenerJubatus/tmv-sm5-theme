return Def.ActorFrame {
	Def.Quad {
		InitCommand=function(self) self:diffuse(Color.Black):zoomto(SCREEN_HEIGHT,SCREEN_WIDTH):Center() end;
		OnCommand=function(self) self:diffusealpha(0):sleep(1.0):decelerate(0.2):diffusealpha(1) end;
	};
	LoadActor(THEME:GetPathG("_logo transition", "base")) .. {
		InitCommand=function(self) self:Center():diffuse(color("#000000")) end;
		OnCommand=function(self) self:zoom(1.1):diffusealpha(0):decelerate(0.3):zoom(1):diffusealpha(1) end;
	};	
	LoadActor(THEME:GetPathG("_logo transition", "top parts")) .. {
		InitCommand=function(self) self:xy(SCREEN_CENTER_X,SCREEN_CENTER_Y-4):diffuse(color("#000000")) end;
		OnCommand=function(self) self:diffusealpha(0):sleep(0.5):decelerate(0.5):diffusealpha(1) end;
	};	
	LoadActor(THEME:GetPathG("_logo transition", "side parts")) .. {
		InitCommand=function(self) self:xy(SCREEN_CENTER_X,SCREEN_CENTER_Y-4):diffuse(color("#000000")) end;
		OnCommand=function(self) self:diffusealpha(0):sleep(0.5+0.2):decelerate(0.5):diffusealpha(1) end;
	};	
	LoadActor(THEME:GetPathG("_logo transition", "middle parts")) .. {
		InitCommand=function(self) self:xy(SCREEN_CENTER_X,SCREEN_CENTER_Y-4):diffuse(color("#000000")) end;
		OnCommand=function(self) self:diffusealpha(0):sleep(0.5+0.4):decelerate(0.5):diffusealpha(1) end;
	};	
	LoadActor(THEME:GetPathG("_logo transition", "bottom parts")) .. {
		InitCommand=function(self) self:xy(SCREEN_CENTER_X,SCREEN_CENTER_Y-4):diffuse(color("#000000")) end;
		OnCommand=function(self) self:diffusealpha(0):sleep(0.5+0.6):decelerate(0.5):diffusealpha(1) end;
	};
};