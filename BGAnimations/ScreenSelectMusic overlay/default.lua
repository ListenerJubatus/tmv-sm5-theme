local t = Def.ActorFrame {};
--t[#t+1] = EXF_ScreenSelectMusic();
--t[#t+1] = Def.ActorFrame{
--	CodeMessageCommand = function(self, params)
--		if params.Name=="ExFolder" then
--			StartEXFolder(GetActiveGroupName());
--		end;
--	end;
--};

local curScreen = Var "LoadingScreen";
local curStage = GAMESTATE:GetCurrentStage();
local curStageIndex = GAMESTATE:GetCurrentStageIndex();

pane_height = 30;
pane_width = 80;

-- Stage
t[#t+1] = Def.ActorFrame {
	InitCommand=function(self)
		self:vertalign(top):x(SCREEN_RIGHT-180):y(SCREEN_TOP+16) 
	end;
	OnCommand=function(self) 
		self:addy(-73):sleep(0.9):decelerate(0.3):addy(73):bouncebegin(0.2):addy(-3):bounceend(0.3):addy(3)
	end;
	OffCommand=function(self)
		self:sleep(0.4):bouncebegin(0.5):addy(-73)
	end;
	LoadActor( THEME:GetPathG("ScreenHeader", "menu flag") ) .. {
		BeginCommand=function(self)
			local top = SCREENMAN:GetTopScreen()
			if top then
				if not string.find(top:GetName(),"ScreenEvaluation") then
					curStageIndex = curStageIndex + 1
				end
			end
			self:playcommand("Set")
		end;
		SetCommand=function(self)
			self:diffuse(StageToColor(curStage));
	    end;
	};			
	LoadActor( THEME:GetPathG("ScreenHeader", "flag overlay") ) .. {
	};
	LoadFont("_decadentafrax 24px") .. {
		InitCommand=function(self)
			self:y(5):zoom(1):strokecolor(color("#000000")):diffuse(color("#FFFFFF")):horizalign(center):shadowlength(1):maxwidth(100)
		end;
		BeginCommand=function(self)
			if SCREENMAN:GetTopScreen() then
				if not string.find(SCREENMAN:GetTopScreen():GetName(),"ScreenEvaluation") then
					curStageIndex = curStageIndex + 1
				end
			end
			self:playcommand("Set")
		end;
		SetCommand=function(self)
		if GAMESTATE:GetCurrentCourse() then
			self:settext( curStageIndex+1 .. " / " .. GAMESTATE:GetCurrentCourse():GetEstimatedNumStages() );
		elseif GAMESTATE:IsEventMode() then
			self:settextf("Free");
		else
			self:settextf("%s", ToEnumShortString(curStage));
	    end;
	    end;
	};
};
-- Sort
t[#t+1] = Def.ActorFrame {
	InitCommand=function(self)
		self:vertalign(top):x(SCREEN_RIGHT-290):y(SCREEN_TOP+16)
	end;
	OnCommand=function(self)
		self:addy(-73):sleep(0.6):decelerate(0.3):addy(73):bouncebegin(0.2):addy(-3):bounceend(0.3):addy(3)
	end;
	OffCommand=function(self)
		self:sleep(0.2):bouncebegin(0.5):addy(-73)
	end;
	LoadActor( THEME:GetPathG("ScreenHeader", "menu flag") ) .. {
		OnCommand=function(self) self:diffuse(color("#137017")) end;
	};			
	LoadActor( THEME:GetPathG("ScreenHeader", "flag overlay") ) .. {
	};
	LoadFont("_decadentafrax 24px") .. {
		InitCommand=function(self)
			self:y(5):zoom(1):strokecolor(color("#000000")):diffuse(color("#FFFFFF")):horizalign(center):shadowlength(1):maxwidth(100)
		end;
        SortOrderChangedMessageCommand=function(self) self:playcommand("Set") end;
        ChangedLanguageDisplayMessageCommand=function(self) self:playcommand("Set") end;
        SetCommand=function(self)
               if GAMESTATE:GetSortOrder() then
					self:finishtweening();
					self:diffusealpha(0);
                    self:settext(SortOrderToLocalizedString(GAMESTATE:GetSortOrder()));
                    self:playcommand("Refresh");
					self:decelerate(0.2):diffusealpha(1);
				else
					self:settext("");
					self:playcommand("Refresh");
               end
        end;
};
};

-- Gotta hang the flags on something.

-- Right pole
t[#t+1] = Def.ActorFrame {
	InitCommand=function(self) self:horizalign(right):x(SCREEN_RIGHT+20):y(SCREEN_TOP+83) end;
	OnCommand=function(self) 
		self:addx(400):sleep(0.2):decelerate(0.6):addx(-400) 
	end;
	OffCommand=function(self) 
		self:sleep(0.2):smooth(0.7):addx(400)
	end;
	Def.ActorFrame {
		Def.Quad {
			InitCommand=function(self)
				self:diffuse(color("#555555")):zoomto(180,200):horizalign(center):vertalign(top)
				self:y(-190):x(-260):blend("BlendMode_NoEffect"):zwrite(true):clearzbuffer(true)
			end;
		};
			-- Jacket view
			Def.ActorFrame {
				InitCommand=function(self) self:ztest(true) end;
				OnCommand=function(self) 
					self:addy(-500):sleep(0.6):decelerate(0.6):addy(500):bouncebegin(0.3):addy(-13):bounceend(0.3):addy(13)
				end;
				LoadActor("_underlay") .. {
				InitCommand=function(self) 
						self:zoom(0.7):horizalign(center):vertalign(top):y(-6):x(-260)
					end;
				};
				-- Jacket sprite
				Def.Sprite {
					name="SongJacket";
					InitCommand=function(self) self:xy(-260,93) end;
					CurrentSongChangedMessageCommand=function(self) self:playcommand("Set") end;
					CurrentCourseChangedMessageCommand=function(self) self:playcommand("Set") end;
					SetCommand=function(self)
						local song = GAMESTATE:GetCurrentSong();
						if song then
							if song:HasJacket() == true then
								self:playcommand("Changed")
								self:Load(song:GetJacketPath())
								self:scaletoclipped(160,160)
								self:finishtweening()							
							elseif song:HasBackground() == true then
								self:playcommand("Changed")
								self:Load(song:GetBackgroundPath())
								self:scaletoclipped(160,160)
								self:finishtweening()	
							elseif song:HasBanner() == true then
								self:playcommand("Changed")
								self:Load(song:GetBannerPath())
								self:scaletoclipped(160,50)
								self:finishtweening()	
							elseif song:HasBanner() == false then
								self:playcommand("Changed")
								self:Load(THEME:GetPathG("Common fallback", "jacket"))
								self:scaletoclipped(160,160)
								self:finishtweening()
							end
						else
							self:Load(THEME:GetPathG("Common fallback", "jacket"))
						end
					end;
					TransitionCommand=function(self)
							if GAMESTATE:GetCurrentSong() then
								if GAMESTATE:GetCurrentSong():HasPreviewVid() then
									self:finishtweening():zoom(1):diffusealpha(1):sleep(1.2):smooth(0.6):diffusealpha(0.6);
								else
									self:finishtweening():linear(0.1):zoom(1):diffusealpha(1);
								end;
							end;
						end;   
					};
					
				-- Preview video	
				Def.ActorFrame {
					ChangedCommand=function(self) self:finishtweening() end;
					CurrentSongChangedMessageCommand=function(self) self:playcommand("Set") end;
					CurrentCourseChangedMessageCommand=function(self) self:playcommand("Set") end;
					ChangedCommand=function(self) self:finishtweening() end;			
					SetCommand=function(self)
						self:playcommand("Changed")
						self:playcommand("Transition")
					end;
					TransitionCommand=function(self)
						if GAMESTATE:GetCurrentSong() then
							if GAMESTATE:GetCurrentSong():HasPreviewVid() then
								self:finishtweening():zoom(1):diffusealpha(0):sleep(1.2):smooth(0.6):diffusealpha(1);
							else
								self:finishtweening():linear(0.1):zoom(1):diffusealpha(1);
							end;
						end;
					end;   
					
					Def.Sprite {
						name="PreviewVideo";
						InitCommand=function(self) self:scaletoclipped(160,90):y(93):x(-260) end;
						CurrentSongChangedMessageCommand=function(self) self:playcommand("Set") end;
						SetCommand=function(self)
							if GAMESTATE:GetCurrentSong() then
								if GAMESTATE:GetCurrentSong():HasPreviewVid() then
									self:visible(false)
									self:sleep(0.01)
									self:Load(GAMESTATE:GetCurrentSong():GetPreviewVidPath())
									self:sleep(0.03)
									self:position(0)
									self:visible(true)
								else
									self:visible(false)
								end
							else
								self:visible(false)
							end
						end;
					};
				};
				-- Overlay
				LoadActor("_jacket base") .. {
				InitCommand=function(self) 
					self:zoom(0.7):horizalign(center):vertalign(top):y(-6):x(-260)
				end;
				};
				StandardDecorationFromFileOptional("BPMDisplay","BPMDisplay");
				LoadFont("_decadentafrax 24px") .. {
				  InitCommand=function(self) 
					self:zoom(0.5):y(198):x(-218+16):horizalign(left):diffuse(color("#FFFFFF")):strokecolor(color("0,0,0,1"))
				  end;	
				  OnCommand=function(self) self:playcommand("Set") end;
				  SetCommand=function(self)
						self:settext("BPM");
					end
				};				
				
				LoadFont("Common Normal") .. {
				 InitCommand=function(self) 
					self:zoom(0.5):y(182):x(-338):horizalign(left):diffuse(color("#FFFFFF")):strokecolor(color("0,0,0,0.5")):maxwidth(310)
				  end;
				  OnCommand=function(self) self:playcommand("Set") end;
				  CurrentSongChangedMessageCommand=function(self) self:finishtweening():playcommand("Set") end;
				  CurrentCourseChangedMessageCommand=function(self) self:finishtweening():playcommand("Set") end;
				  ChangedLanguageDisplayMessageCommand=function(self) self:finishtweening():playcommand("Set") end;
				  SetCommand=function(self)
					   if GAMESTATE:GetCurrentSong() then
							if GAMESTATE:GetCurrentSong():GetGenre() == "" then
								self:settext("N/A");
								self:diffusealpha(0.5);
							else
								self:settext(GAMESTATE:GetCurrentSong():GetGenre()); 
								self:diffusealpha(1);
							end;
							self:playcommand("Refresh");
						else
							self:settext("N/A");
							self:diffusealpha(0.5);
							self:playcommand("Refresh");
					   end
				  end;
				};
						
				StandardDecorationFromFileOptional("DifficultyList","DifficultyList");	
				StandardDecorationFromFileOptional("SongTime","SongTime") .. {
					SetCommand=function(self)
						local curSelection = nil;
						local length = 0.0;
						if GAMESTATE:IsCourseMode() then
							curSelection = GAMESTATE:GetCurrentCourse();
							self:playcommand("Reset");
							if curSelection then
								local trail = GAMESTATE:GetCurrentTrail(GAMESTATE:GetMasterPlayerNumber());
								if trail then
									length = TrailUtil.GetTotalSeconds(trail);
								else
									length = 0.0;
								end;
							else
								length = 0.0;
							end;
						else
							curSelection = GAMESTATE:GetCurrentSong();
							self:playcommand("Reset");
							if curSelection then
								length = curSelection:MusicLengthSeconds();
								if curSelection:IsLong() then
									self:playcommand("Long");
								elseif curSelection:IsMarathon() then
									self:playcommand("Marathon");
								else
									self:playcommand("Reset");
								end
							else
								length = 0.0;
								self:playcommand("Reset");
							end;
						end;
						self:settext( SecondsToMSS(length) );
					end;
					CurrentSongChangedMessageCommand=function(self) self:playcommand("Set") end;
					CurrentCourseChangedMessageCommand=function(self) self:playcommand("Set") end;
					CurrentTrailP1ChangedMessageCommand=function(self) self:playcommand("Set") end;
					CurrentTrailP2ChangedMessageCommand=function(self) self:playcommand("Set") end;
				};	
			};
			
		LoadActor("right pole") .. {
			InitCommand=function(self) self:zoom(0.75):addx(-147) end;
		};	
		-- Mask	

		LoadActor("_jacket top") .. {
			InitCommand=function(self) 
				self:zoom(0.7):horizalign(center):vertalign(top):y(-6):x(-260)
			end;
		};
	};
	-- P2 Difficulty Banner
	LoadActor("statsDisplay",PLAYER_2) .. {
		InitCommand=function(self) self:x(-184) end;
	};
};


t[#t+1] = Def.ActorFrame {
	InitCommand=function(self) self:horizalign(left):x(SCREEN_LEFT-8):y(SCREEN_TOP+83) end;
	OnCommand=function(self) self:addx(-400):sleep(0.2):decelerate(0.6):addx(400) end;
	OffCommand=function(self) self:sleep(0.2):smooth(0.7):addx(-400) end;
	LoadActor("left pole") .. {
			InitCommand=function(self) self:zoom(0.75):horizalign(left):addx(-69) end;
	};	
	LoadActor("statsDisplay",PLAYER_1);
	};


t[#t+1] = Def.ActorFrame{
	Def.Quad{
		InitCommand=function(self) self:Center():draworder(160):zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):diffuse(color("#000000")):diffusealpha(0) end;
		ShowPressStartForOptionsCommand=function(self) self:decelerate(0.4):diffusealpha(0.75) end;
		ShowEnteringOptionsCommand=function(self) self:sleep(0.2):decelerate(0.5):diffusealpha(0) end;
		HidePressStartForOptionsCommand=function(self) self:sleep(0.2):decelerate(0.5):diffusealpha(0) end;
	};	
	Def.Quad{
		InitCommand=function(self) self:Center():draworder(160):zoomto(SCREEN_WIDTH,76):diffuse(color("#492A51")):diffusealpha(0) end;
		ShowPressStartForOptionsCommand=function(self) self:decelerate(0.4):diffusealpha(0.75) end;
		ShowEnteringOptionsCommand=function(self) self:sleep(0.2):decelerate(0.5):diffusealpha(0) end;
		HidePressStartForOptionsCommand=function(self) self:sleep(0.2):decelerate(0.5):diffusealpha(0) end;
	};
};

t[#t+1] = StandardDecorationFromFileOptional("SongOptions","SongOptionsText") .. {
	ShowPressStartForOptionsCommand=THEME:GetMetric(Var "LoadingScreen","SongOptionsShowCommand");
	ShowEnteringOptionsCommand=THEME:GetMetric(Var "LoadingScreen","SongOptionsEnterCommand");
	HidePressStartForOptionsCommand=THEME:GetMetric(Var "LoadingScreen","SongOptionsHideCommand");
};

return t;