local iPN = ...;
local function PercentScore(pn)
	local t = LoadFont("Common Medium")..{
		InitCommand=function(self)
			self:zoom(1):diffuse(Color("White")):strokecolor(color("0,0,0,0.6")):diffusealpha(1):horizalign(right)
		end;
		BeginCommand=function(self) self:playcommand("Set") end;
		SetCommand=function(self)
			local SongOrCourse, StepsOrTrail;
			if GAMESTATE:IsCourseMode() then
				SongOrCourse = GAMESTATE:GetCurrentCourse();
				StepsOrTrail = GAMESTATE:GetCurrentTrail(pn);
			else
				SongOrCourse = GAMESTATE:GetCurrentSong();
				StepsOrTrail = GAMESTATE:GetCurrentSteps(pn);
			end;

			local profile, scorelist;
			local text = "";
			if SongOrCourse and StepsOrTrail then
				if PROFILEMAN:IsPersistentProfile(pn) then
					-- player profile
					profile = PROFILEMAN:GetProfile(pn);
				else
					-- machine profile
					profile = PROFILEMAN:GetMachineProfile();
				end;

				scorelist = profile:GetHighScoreList(SongOrCourse,StepsOrTrail);
				assert(scorelist)
				local scores = scorelist:GetHighScores();
				local topscore = scores[1];
				if topscore then
					text = string.format("%.2f%%", topscore:GetPercentDP()*100.0);
					-- 100% hack
					if text == "100.00%" then
						text = "100%";
					end;
				else
					text = string.format("%.2f%%", 0);
				end;
			else
				text = "";
			end;
			self:settext(text);
		end;
		CurrentSongChangedMessageCommand=function(self) self:playcommand("Set") end;
		CurrentCourseChangedMessageCommand=function(self) self:playcommand("Set") end;
	};

	if pn == PLAYER_1 then
		t.CurrentStepsP1ChangedMessageCommand=function(self) self:playcommand("Set") end;
		t.CurrentTrailP1ChangedMessageCommand=function(self) self:playcommand("Set") end;
	else
		t.CurrentStepsP2ChangedMessageCommand=function(self) self:playcommand("Set") end;
		t.CurrentTrailP2ChangedMessageCommand=function(self) self:playcommand("Set") end;
	end

	return t;
end;

local t = Def.ActorFrame {};
t[#t+1] = Def.ActorFrame {
	-- Difficulty Banner
	Def.ActorFrame {
		InitCommand=function(self) self:zoom(0.7):horizalign(center):vertalign(top):xy(86,-6) end;
		LoadActor("_banner base") .. {
			InitCommand=function(self) self:horizalign(center):vertalign(top):diffuse(PlayerColor(iPN)) end;
		};		
		LoadActor("_banner color") .. {
			InitCommand=function(self) self:horizalign(center):vertalign(top) end;
			["CurrentSteps"..ToEnumShortString(iPN).."ChangedMessageCommand"]=function(self) self:queuecommand("Set") end; 
				PlayerJoinedMessageCommand=function(self) self:queuecommand("Set"):diffusealpha(0):decelerate(0.3):diffusealpha(1) end;
				ChangedLanguageDisplayMessageCommand=function(self) self:queuecommand("Set") end;
				SetCommand=function(self)
						if GAMESTATE:GetCurrentSong() then 
							if GAMESTATE:GetCurrentSteps(iPN) ~= nil then
								self:finishtweening():linear(0.2)
								self:diffuse(ColorMidTone(CustomDifficultyToColor(GetCustomDifficulty(GAMESTATE:GetCurrentSteps(iPN):GetStepsType(), GAMESTATE:GetCurrentSteps(iPN):GetDifficulty(), GAMESTATE:IsCourseMode() and SongOrCourse:GetCourseType() or nil))))
							else
							end
						else
						end
					end
		};
		-- Number 
			LoadFont("ScreenSelectMusic diffmeter") .. { 
			  InitCommand=function(self) self:y(90-8):zoom(1.25):horizalign(center):maxwidth(130) end;
			  OnCommand=function(self)
					if ThemePrefs.Get("AutoSetStyle") == false then
						self:addy(10)
					end;
				self:diffusealpha(0):smooth(0.2):diffusealpha(1)
			  end;
			  ["CurrentSteps"..ToEnumShortString(iPN).."ChangedMessageCommand"]=function(self) self:queuecommand("Set"):queuecommand("Transition") end;
			  PlayerJoinedMessageCommand=function(self) self:queuecommand("Set"):diffusealpha(0):smooth(0.3):diffusealpha(1) end;
			  ChangedLanguageDisplayMessageCommand=function(self) self:queuecommand("Set") end;
			  TransitionCommand=function(self) self:finishtweening():diffusealpha(0):decelerate(0.2):diffusealpha(1) end;
			  SetCommand=function(self)
				if GAMESTATE:GetCurrentSong() then 
					if GAMESTATE:GetCurrentSteps(iPN) ~= nil then
						self:settext(GAMESTATE:GetCurrentSteps(iPN):GetMeter())
							if GAMESTATE:GetCurrentSteps(iPN):GetMeter() > 18 then
								self:diffuse(color("#FC7C7C"));
								self:diffusebottomedge(color("#D73A3A"));	
								self:strokecolor(color("#B00C0C"));
							elseif GAMESTATE:GetCurrentSteps(iPN):GetMeter() > 15 then
								self:diffuse(color("#FCB988"));
								self:diffusebottomedge(color("#DB782F"));
								self:strokecolor(color("#B0500C"));
							else
								self:diffuse(color("#FFFFFF"));
								self:diffusebottomedge(color("#C4C4C4"));
								self:strokecolor(ColorDarkTone(CustomDifficultyToColor(GetCustomDifficulty(GAMESTATE:GetCurrentSteps(iPN):GetStepsType(), GAMESTATE:GetCurrentSteps(iPN):GetDifficulty(), GAMESTATE:IsCourseMode() and SongOrCourse:GetCourseType() or nil))));
							end
					else
						self:settext("")
					end
				else
					self:settext("")
				end
			  end
			};
		-- Difficulty name
			LoadFont("Common Medium") .. { 
				InitCommand=function(self) self:y(127-8):maxwidth(180):uppercase(true) end;
				OnCommand=function(self)
					if ThemePrefs.Get("AutoSetStyle") == false then
						self:addy(10)
					end;
					self:diffusealpha(0):smooth(0.2):diffusealpha(1)
				end;			  
				["CurrentSteps"..ToEnumShortString(iPN).."ChangedMessageCommand"]=function(self) self:queuecommand("Set"):queuecommand("Transition") end;
				PlayerJoinedMessageCommand=function(self)
					self:queuecommand("Set"):diffusealpha(0):smooth(0.3):diffusealpha(1)
				end;
				ChangedLanguageDisplayMessageCommand=function(self) self:queuecommand("Set") end;
				TransitionCommand=function(self)
					self:finishtweening():diffusealpha(0):decelerate(0.2):diffusealpha(1)
				end;
				SetCommand=function(self)
				local steps_data = GAMESTATE:GetCurrentSteps(iPN)
				local song = GAMESTATE:GetCurrentSong();
				if song then 
					if steps_data ~= nil then
						local st = steps_data:GetStepsType();
						local diff = steps_data:GetDifficulty();
						local courseType = GAMESTATE:IsCourseMode() and SongOrCourse:GetCourseType() or nil;
						local cd = GetCustomDifficulty(st, diff, courseType);
							if steps_data:IsAnEdit() then
								self:settext(steps_data:GetChartName())
							else
								self:settext(THEME:GetString("CustomDifficulty",ToEnumShortString(diff)));
							end;
						self:strokecolor(ColorDarkTone(CustomDifficultyToColor(cd)));
					else
						self:settext("")
					end
				else
					self:settext("")
				end
				end
			};				
		-- Style name
		LoadFont("Common Normal") .. { 
			InitCommand=function(self)
				self:zoom(0.75):y(152-12):maxwidth(130):uppercase(true):visible(ThemePrefs.Get("AutoSetStyle")) 
			end;
			OnCommand=function(self)
				self:diffusealpha(0):smooth(0.2):diffusealpha(1)
			end;
			["CurrentSteps"..ToEnumShortString(iPN).."ChangedMessageCommand"]=function(self) self:queuecommand("Set"):queuecommand("Transition") end;
			PlayerJoinedMessageCommand=function(self)
				self:queuecommand("Set"):diffusealpha(0):smooth(0.3):diffusealpha(1)
			end;
			ChangedLanguageDisplayMessageCommand=function(self) self:queuecommand("Set") end;
			TransitionCommand=function(self) self:finishtweening():diffusealpha(0):decelerate(0.2):diffusealpha(1) end;
			SetCommand=function(self)
			local steps_data = GAMESTATE:GetCurrentSteps(iPN)
			local song = GAMESTATE:GetCurrentSong();
			if song then 
				if steps_data ~= nil then
					local st = steps_data:GetStepsType();
					local diff = steps_data:GetDifficulty();
					local courseType = GAMESTATE:IsCourseMode() and SongOrCourse:GetCourseType() or nil;
					local cd = GetCustomDifficulty(st, diff, courseType);
					self:settext(THEME:GetString("StepsType",ToEnumShortString(st)));
					self:strokecolor(ColorDarkTone(CustomDifficultyToColor(cd)));
				else
					self:settext("")
				end
			else
				self:settext("")
			end
		  end
		};	
		LoadFont("Common Medium") .. {
		  InitCommand=function(self)
				self:zoom(0.75):y(175):x(-90):horizalign(left):diffuse(color("#FFFFFF")):strokecolor(color("0,0,0,0.6"))
				self:visible(GAMESTATE:IsHumanPlayer(iPN))
			end;
		  PlayerJoinedMessageCommand=function(self,param)
					if param.Player == iPN then
						self:visible(true):diffusealpha(0):smooth(0.3):diffusealpha(1)
					end;
			end;
		  OnCommand=function(self) self:playcommand("Set") end;
          SetCommand=function(self)
				self:settext(string.upper(THEME:GetString("PaneDisplay","MachineHigh")) .. ":");
			end
		};
		StandardDecorationFromTable("PercentScore"..ToEnumShortString(iPN), PercentScore(iPN)) .. {
			InitCommand=function(self)
				self:zoom(0.75):x(24):y(175):maxwidth(120) 
			end;	
		};
		Def.Sprite	{
			InitCommand=function(self) self:xy(64,175):zoom(0.5) end;
			OnCommand=function(self) self:playcommand("Set") end;
			["CurrentSteps"..ToEnumShortString(iPN).."ChangedMessageCommand"]=function(self) self:playcommand("Set") end;
			CurrentSongChangedMessageCommand=function(self) self:playcommand("Set") end;
			PlayerJoinedMessageCommand=function(self) self:playcommand("Set") end;
			ChangedLanguageDisplayMessageCommand=function(self) self:playcommand("Set") end;
			SetCommand=function(self)
				local steps = GAMESTATE:GetCurrentSteps(iPN)
				local song = GAMESTATE:GetCurrentSong();
				if song then
					if steps ~= nil then
						local score = PROFILEMAN:GetProfile(iPN):GetHighScoreList(song,steps):GetHighScores()
						local getscore = score[1]
						if getscore then
							showscore = getscore:GetGrade()
							if showscore ~= nil then
								self:Load(THEME:GetPathG("GradeDisplay Grade", showscore))
								self:visible(true)
							else
								self:visible(false)
							end
						else
							self:visible(false)
						end
					end
				end	
			end;
		};	
		-- Stats
		LoadActor(THEME:GetPathG("PaneDisplay","Text"),iPN).. {
			InitCommand=function(self) 
				self:x(28):y(210)
			end;
		};
		-- Hands/Lifts
		Def.Sprite {
			Texture=THEME:GetPathG("PaneDisplay", "hands marker");
			InitCommand=function(self)
				self:horizalign(center):x(-42):y(370):blend('add')
			end;
			["CurrentSteps"..ToEnumShortString(iPN).."ChangedMessageCommand"]=function(self)
			local steps_data = GAMESTATE:GetCurrentSteps(iPN)
				if steps_data ~= nil then	
					local radar_data = steps_data:GetRadarValues(iPN);
					if radar_data:GetValue('RadarCategory_Hands') ~= 0 then
						self:diffusealpha(1)
					else
						self:diffusealpha(0.2)
					end
				else
					self:diffusealpha(0.2)
				end;
			end			
		};			
		
		Def.Sprite {
			Texture=THEME:GetPathG("PaneDisplay", "lifts marker");
			InitCommand=function(self)
				self:horizalign(center):x(42):y(370):blend('add')
			end;
			["CurrentSteps"..ToEnumShortString(iPN).."ChangedMessageCommand"]=function(self)
			local steps_data = GAMESTATE:GetCurrentSteps(iPN)
				if steps_data ~= nil then	
					local radar_data = steps_data:GetRadarValues(iPN);
					if radar_data:GetValue('RadarCategory_Lifts') ~= 0 then
						self:diffusealpha(1)
					else
						self:diffusealpha(0.2)
					end
				else
					self:diffusealpha(0.2)
				end;
			end			
		};		
		-- Show when not joined
		Def.ActorFrame {
			InitCommand=function(self)
				self:horizalign(center):vertalign(top):y(220):visible(not GAMESTATE:IsHumanPlayer(iPN))
			end	;
			PlayerJoinedMessageCommand=function(self,param)
				if param.Player == iPN then
					self:decelerate(0.3):diffusealpha(0)
					end;
				end;	
			LoadActor("_noplayer base") .. {
				OnCommand=function(self) self:diffuse(ColorDarkTone(PlayerColor(iPN))) end;
			};
			LoadFont("_decadentafrax 48px") .. { 
				InitCommand=function(self)
					self:y(-30):wrapwidthpixels(190):vertspacing(-36):zoom(1):strokecolor(color("0,0,0,0.3"))
				end;
				OnCommand=function(self)
					self:playcommand("Set"):diffuseshift():effectcolor1(1,1,1,1):effectcolor2(1,1,1,0.6):effectperiod(2.4)
				end;
				PlayerJoinedMessageCommand=function(self,param)
				if param.Player == iPN then
					self:stopeffect():visible(false)
					end;
				end;
				SetCommand=function(self)
					self:settext(THEME:GetString("ScreenSelectMusic","Start To Join"));
				end
			};	
		};
		-- End show when not joined
	};
};
return t;