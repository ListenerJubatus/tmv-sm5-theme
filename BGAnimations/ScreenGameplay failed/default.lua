if not GAMESTATE:IsCourseMode() then
	local song = GAMESTATE:GetCurrentSong():GetDisplayFullTitle();
	if currentSong == 'Dogsong' then
		 failgraphic = 'dogtime'
		else
		 failgraphic = 'sign'
	end;
else 
	failgraphic = 'course'
end;

return Def.ActorFrame {
	Def.Quad {
		InitCommand=function(self) self:Center():zoomto(SCREEN_WIDTH,SCREEN_HEIGHT) end;
		OnCommand=function(self) self:blend(Blend.Multiply):smooth(1):diffusealpha(0.2):diffuse(color("#423E3A")) end;
	};

	-- Black BG
	Def.Quad {
		InitCommand=function(self) self:Center():zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):diffuse(Color.Black):diffusealpha(0) end;
		OnCommand=function(self) self:sleep(1.1):smooth(5):diffuse(Color.Black) end;
	};

		
	Def.Quad {
		InitCommand=function(self) self:zoomto(SCREEN_WIDTH,160):horizalign(center):Center():diffuse(color("0,0,0,0.6")):fadeleft(0.3):faderight(0.3) end;
		OnCommand=function(self) self:diffusealpha(0):sleep(0.3):smooth(1):diffusealpha(1) end;
	};
		
	LoadActor( THEME:GetPathG("_failed", failgraphic ) ) .. {
		InitCommand=function(self) self:horizalign(center):Center():zoom(0.75) end;
		OnCommand=function(self) self:diffusealpha(0):sleep(0.3):zoom(0.5):decelerate(2):zoom(0.75):diffusealpha(1):sleep(3.7):smooth(0.6):diffusealpha(0) end;
	};

	-- Opening flash	

	LoadActor("_shutdown") .. {
		OnCommand=function(self) self:queuecommand("Play") end;
		PlayCommand=function(self) self:play() end;
	};
};