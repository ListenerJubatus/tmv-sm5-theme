local t = Def.ActorFrame {};
-- A very useful table...
local eval_lines = {
	"W1",
	"W2",
	"W3",
	"W4",
	"W5",
	"Miss",
	"Held",
	"MaxCombo"
}

local grade_area_offset = 16
local fade_out_speed = 0.3
local fade_out_pause = 0.08

-- And a function to make even better use out of the table.
local function GetJLineValue(line, pl)
	if line == "Held" then
		return STATSMAN:GetCurStageStats():GetPlayerStageStats(pl):GetHoldNoteScores("HoldNoteScore_Held")
	elseif line == "MaxCombo" then
		return STATSMAN:GetCurStageStats():GetPlayerStageStats(pl):MaxCombo()
	else
		return STATSMAN:GetCurStageStats():GetPlayerStageStats(pl):GetTapNoteScores("TapNoteScore_" .. line)
	end
	return "???"
end

-- You know what, we'll deal with getting the overall scores with a function too.
local function GetPlScore(pl, scoretype)
	local primary_score = FormatPercentScore(STATSMAN:GetCurStageStats():GetPlayerStageStats(pl):GetPercentDancePoints())
	local secondary_score = STATSMAN:GetCurStageStats():GetPlayerStageStats(pl):GetScore()
	
	if scoretype == "primary" then
		return primary_score
	else
		return secondary_score
	end
end

-- #################################################
-- Time to deal with all of the player stats. ALL OF THEM.

local eval_parts = Def.ActorFrame {}

for ip, p in ipairs(GAMESTATE:GetHumanPlayers()) do
	-- Some things to help positioning
	local flag_pos = string.find(p, "P1") and SCREEN_LEFT+126 or SCREEN_RIGHT-126
	local step_count_offs = string.find(p, "P1") and SCREEN_LEFT+24 or SCREEN_RIGHT-24
	local grade_parts_offs = string.find(p, "P1") and SCREEN_LEFT+102 or SCREEN_RIGHT-102
	local text_alignment = string.find(p, "P1") and left or right
	local value_alignment = string.find(p, "P1") and right or left
	local value_offset = string.find(p, "P1") and 1 or -1
	local anim_offset = string.find(p, "P1") and -1 or 1
	local p_grade = STATSMAN:GetCurStageStats():GetPlayerStageStats(p):GetGrade()

	eval_parts[#eval_parts+1] = LoadActor(THEME:GetPathG("ScreenEvaluation", "results pole")) .. {
		InitCommand=function(self)
			self:horizalign(center)
			self:x(flag_pos):y(SCREEN_CENTER_Y):zoomx(value_offset)
		end;
		OnCommand=function(self) self:addx(400*anim_offset):decelerate(1.0):addx(400*value_offset) end;
		OffCommand=function(self)
			self:sleep(0.1):decelerate(0.8):addx(400*anim_offset)
		end;
	};	
	eval_parts[#eval_parts+1] = LoadActor(THEME:GetPathG("ScreenEvaluation", "results flag")) .. {
		InitCommand=function(self)
			self:horizalign(center):diffuse(PlayerColor(p))
			self:x(flag_pos):y(SCREEN_CENTER_Y):zoomx(value_offset)
		end;
		OnCommand=function(self) self:addx(400*anim_offset):decelerate(1.0):addx(400*value_offset) end;
		OffCommand=function(self)
			self:sleep(0.1):decelerate(0.8):addx(400*anim_offset)
		end;
	};

	-- Step counts.
	for i, v in ipairs(eval_lines) do
		local spacing = 21*i
		local cur_line = "JudgmentLine_" .. v

		eval_parts[#eval_parts+1] = Def.ActorFrame {
		InitCommand=function(self) 	self:y((SCREEN_CENTER_Y-58)+(spacing)) end;
		OnCommand=function(self) self:addx(400*anim_offset):decelerate(1.0):addx(400*value_offset) end;
		OffCommand=function(self)
			self:sleep(0.1):decelerate(0.8):addx(400*anim_offset)
		end;
			-- Item name
			Def.BitmapText {
				Font = "Evaluation Labels",
				Text = string.upper(JudgmentLineToLocalizedString(cur_line));
				InitCommand=function(self) 
					self:x(step_count_offs):zoom(0.75):horizalign(text_alignment):shadowlength(1)
					self:diffuse(ColorLightTone(JudgmentLineToColor(cur_line))):strokecolor(ColorDarkTone(JudgmentLineToColor(cur_line)))		
				end;
			};
			-- Value
			Def.BitmapText {
				Font = "Evaluation Lines",
				InitCommand=function(self)
					self:x(step_count_offs+(160*value_offset)):maxwidth(120):horizalign(value_alignment):zoom(0.75)
					self:diffuse(PlayerColor(p)):diffusetopedge(ColorLightTone(PlayerColor(p))):strokecolor(ColorDarkTone(PlayerColor(p)))
				end;
				Text=GetJLineValue(v, p);
			};
		};
	end
	
	-- Letter grade and associated parts.
	eval_parts[#eval_parts+1] = Def.Sprite{
		Texture=THEME:GetPathG("GradeDisplay", "Grade " .. p_grade);
		InitCommand=function(self)
			self:horizalign(center):x(grade_parts_offs):y(_screen.cy-92)		   
			self:zoom(1)
		end;
		OnCommand=function(self)
				self:diffusealpha(0):sleep(1):zoom(1.2):rotationz(20*value_offset):decelerate(0.3):zoom(0.9)
				:diffusealpha(1.25):rotationz(0):decelerate(0.3):zoom(1)
				if STATSMAN:GetCurStageStats():GetPlayerStageStats(p):GetStageAward() then
				  self:sleep(0.3):smooth(0.4):addy(-10);
				end;
			end;
		OffCommand=function(self)
			self:sleep(0.1):decelerate(0.8):addx(400*anim_offset)
		end;
	};
	
	-- FC status
	
	eval_parts[#eval_parts+1] = Def.Sprite {
		InitCommand=function(self) 
			self:horizalign(center):x(grade_parts_offs+(90*value_offset)):y(_screen.cy-92)
			self:visible(not STATSMAN:GetCurStageStats():GetPlayerStageStats(p):GetFailed())
		end;
		OnCommand=function(self)
			self:playcommand("Set")
			self:diffusealpha(0):sleep(1):zoom(0.5):rotationz(20*value_offset):decelerate(0.3):zoom(0.8)
			:diffusealpha(1):rotationz(0):decelerate(0.3):zoom(0.75)
		end;
		SetCommand=function(self)
			if(STATSMAN:GetCurStageStats():GetPlayerStageStats(p):FullComboOfScore('TapNoteScore_W1') == true) then
				self:Load(THEME:GetPathG("ScreenEvaluation", "W1 FC"))
			elseif(STATSMAN:GetCurStageStats():GetPlayerStageStats(p):FullComboOfScore('TapNoteScore_W2') == true) then
				self:Load(THEME:GetPathG("ScreenEvaluation", "W2 FC"))
			elseif(STATSMAN:GetCurStageStats():GetPlayerStageStats(p):FullComboOfScore('TapNoteScore_W3') == true) then
				self:Load(THEME:GetPathG("ScreenEvaluation", "W3 FC"))
			end;
		end;
		OffCommand=function(self)
			self:sleep(0.1):decelerate(0.8):addx(400*anim_offset)
		end;
	};

	-- Primary score.
	eval_parts[#eval_parts+1] = Def.BitmapText {
		Font = "Evaluation Lines",
		InitCommand=function(self)
			self:horizalign(value_alignment):x(step_count_offs+(160*value_offset)):y(_screen.cy+138)
			self:diffuse(PlayerColor(p)):diffusetopedge(ColorLightTone(PlayerColor(p))):strokecolor(ColorDarkTone(PlayerColor(p)))
			:zoom(1):shadowlength(1):maxwidth(140)
		end;
		Text=GetPlScore(p, "primary");
		OnCommand=function(self)
			self:diffusealpha(0):sleep(1.1):decelerate(0.3):diffusealpha(1)
		end;
		OffCommand=function(self)
			self:linear(0.1):diffusealpha(0)
		end;
	}		
	
	eval_parts[#eval_parts+1] = Def.BitmapText {
		Font = "Common Normal",
		InitCommand=function(self)
			self:horizalign(value_alignment):x(step_count_offs+(160*value_offset)):y(_screen.cy+138+20)
			self:diffuse(ColorLightTone(PlayerColor(p))):diffusetopedge(color("#FFFFFF")):strokecolor(ColorDarkTone(PlayerColor(p)))
			:zoom(0.5):shadowlength(1):maxwidth(140)
		end;
		OnCommand=function(self)
			local record = STATSMAN:GetCurStageStats():GetPlayerStageStats(p):GetPersonalHighScoreIndex()
			local hasPersonalRecord = record ~= -1
			self:visible(hasPersonalRecord);
			local text = string.format(THEME:GetString("ScreenEvaluation", "PersonalRecord"), record+1)
			self:settext(text):diffusealpha(0):sleep(1.1):decelerate(0.3):diffusealpha(1)
		end;
		OffCommand=function(self)
			self:linear(0.1):diffusealpha(0)
		end;
	};		
	
	eval_parts[#eval_parts+1] = Def.BitmapText {
		Font = "Common Normal",
		InitCommand=function(self)
			self:horizalign(value_alignment):x(step_count_offs+(80*value_offset)):y(_screen.cy+138+20)
			self:diffuse(ColorLightTone(PlayerColor(p))):diffusetopedge(color("#FFFFFF")):strokecolor(ColorDarkTone(PlayerColor(p)))
			:zoom(0.5):shadowlength(1):maxwidth(140)
		end;
		OnCommand=function(self)
			local record = STATSMAN:GetCurStageStats():GetPlayerStageStats(p):GetMachineHighScoreIndex()
			local hasMachineRecord = record ~= -1
			self:visible(hasMachineRecord);
			local text = string.format(THEME:GetString("ScreenEvaluation", "MachineRecord"), record+1)
			self:settext(text):diffusealpha(0):sleep(1.1):decelerate(0.3):diffusealpha(1)
		end;
		OffCommand=function(self)
			self:linear(0.1):diffusealpha(0)
		end;
	};	
	
	eval_parts[#eval_parts+1] = Def.BitmapText {
		Font = "Evaluation Award",
		InitCommand=function(self)
			self:horizalign(center):x(grade_parts_offs):y(_screen.cy-67)
			self:diffuse(color("#FFFFFF")):diffusetopedge(ColorLightTone(PlayerColor(p)))
			:zoom(1):shadowlength(1):maxwidth(140)
		end;
			OnCommand=function(self)
				if STATSMAN:GetCurStageStats():GetPlayerStageStats(p):GetStageAward() then
					self:settext(THEME:GetString( "StageAward", ToEnumShortString(STATSMAN:GetCurStageStats():GetPlayerStageStats(p):GetStageAward()) ))
					self:diffusealpha(0):sleep(2):smooth(0.3):diffusealpha(1)
				end;
			end;
		OffCommand=function(self)
			self:sleep(0.1):decelerate(0.8):addx(400*anim_offset)
		end;
	}
	
	eval_parts[#eval_parts+1] = Def.BitmapText {
		Font = "Common Normal",
		InitCommand=function(self)
			self:vertalign(bottom):horizalign(value_alignment):x(step_count_offs+(160*value_offset)):y(_screen.cy+188):wrapwidthpixels(176/0.4)
			self:diffuse(Color.White):zoom(0.4):diffusealpha(0.8)
		end;
		OnCommand=function(self) self:addx(400*anim_offset):decelerate(1.0):addx(400*value_offset) end;
		Text=GAMESTATE:GetPlayerState(p):GetPlayerOptionsString(0);
		OffCommand=function(self)
			self:sleep(0.1):decelerate(0.8):addx(400*anim_offset)
		end;
	};
end

t[#t+1] = eval_parts;

for ip, p in ipairs(GAMESTATE:GetHumanPlayers()) do
	-- Some things to help positioning
	local grade_parts_offs = string.find(p, "P1") and SCREEN_LEFT+102 or SCREEN_RIGHT-102
	local value_offset = string.find(p, "P1") and 1 or -1
	local anim_offset = string.find(p, "P1") and -1 or 1	
	t[#t+1] = LoadFont("Common Medium") .. {
	InitCommand=function(self)
		self:horizalign(center):xy(grade_parts_offs,_screen.cy-145)		   
		self:diffuse(color("#FFFFFF")):zoom(0.65):maxwidth(160)
	end;
	OnCommand=function(self) 
		self:playcommand("Set") 
		self:addx(400*anim_offset):decelerate(1.0):addx(400*value_offset)
	end;
	["CurrentSteps"..ToEnumShortString(p).."ChangedMessageCommand"]=function(self) self:playcommand("Set") end;
	ChangedLanguageDisplayMessageCommand=function(self) self:playcommand("Set") end;
	SetCommand=function(self)
		if GAMESTATE:IsCourseMode() ~= true then
			local steps_data = GAMESTATE:GetCurrentSteps(p)
			local song = GAMESTATE:GetCurrentSong();
			if song and steps_data ~= nil then
				local st = steps_data:GetStepsType();
				local diff = steps_data:GetDifficulty();
				local courseType = GAMESTATE:IsCourseMode() and SongOrCourse:GetCourseType() or nil;
				local cd = GetCustomDifficulty(st, diff, courseType);
				self:settext((THEME:GetString("CustomDifficulty",ToEnumShortString(diff))) .. "  " .. steps_data:GetMeter());				
				self:diffuse(color("#FFFFFF")):diffusebottomedge(ColorLightTone(CustomDifficultyToColor(cd)))
			end
		else
			local course = GAMESTATE:GetCurrentCourse();
			local steps_data = GAMESTATE:GetCurrentTrail(p);
			if course and steps_data ~= nil then
				local st = steps_data:GetStepsType();
				local diff = steps_data:GetDifficulty();
				local courseType = course:GetCourseType();
				local cd = GetCustomDifficulty(st, diff, courseType);
				self:settext(THEME:GetString("CustomDifficulty",ToEnumShortString(diff)));
				self:diffuse(color("#FFFFFF")):diffusebottomedge(ColorLightTone(CustomDifficultyToColor(cd)))
			end
		end;
	end;
	OffCommand=function(self)
		self:sleep(0.1):decelerate(0.8):addx(400*anim_offset)
	end;
	};		
end;
return t