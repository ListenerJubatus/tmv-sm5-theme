local raveChildren
if not GAMESTATE:IsCourseMode() then
return Def.ActorFrame {
	LoadActor("_scores");
	Def.ActorFrame {
		InitCommand=function(self)
			self:xy(SCREEN_CENTER_X,SCREEN_CENTER_Y+160)
		end;
		OnCommand=function(self)
			self:zoomy(0):diffusealpha(0):sleep(0.5):decelerate(0.4):diffusealpha(1):zoomy(1)
		end;
		OffCommand=function(self)
			self:decelerate(0.8):addy(200):zoomy(0)
		end;
		
			Def.Sprite {
				name="SongBanner";
				OnCommand=function(self) 
					 local song =GAMESTATE:GetCurrentSong(); 
						if song then
							if song:HasBanner() then
								self:Load(song:GetBannerPath()):scaletoclipped(256,80)
							else
								self:Load(THEME:GetPathG("Common fallback", "banner")):scaletoclipped(256,80)
							end
							self:zoom(0.75)
						else
							self:diffusealpha(0)
					end
				end;
			};
			LoadActor(THEME:GetPathG("ScreenEvaluationSummary", "BannerFrame")) .. {
				OnCommand=function(self)
					self:zoom(0.75)
				end;	
			};
		};		
		LoadFont("Common Normal") .. {
			InitCommand=function(self) self:horizalign(center):x(SCREEN_CENTER_X):y(SCREEN_BOTTOM-120):zoom(0.5):diffuse(color("#C4E9F5")):strokecolor(color("#000000")):maxwidth(760) end;
			OnCommand=function(self) self:playcommand('Set'):diffusealpha(0):sleep(0.3):smooth(0.3):diffusealpha(1) end;
			OffCommand=function(self) self:smooth(0.2):diffusealpha(0) end;
			SetCommand=function(self)
				self:settext(GAMESTATE:GetCurrentSong():GetDisplayFullTitle())
					if GAMESTATE:GetCurrentSong():HasBanner() then
						self:visible(false);
					else
						self:visible(true);
					end
				end;
		};
	LoadActor(THEME:GetPathS("ScreenEvaluation", "flags")) .. {
		OnCommand=function(self)
			self:play();
		end;
	};	
	Def.Quad {
		InitCommand=function(self) self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):Center():diffuse(color("#000000")):diffusealpha(0)
		end;
		OffCommand=function(self) self:sleep(1.2):decelerate(0.4):diffusealpha(1):sleep(0.2)
		end;
	};
};
else

return Def.ActorFrame {
	LoadActor("_scores");

	LoadFont("Common Normal") .. {
		InitCommand=function(self) self:horizalign(center):x(SCREEN_CENTER_X):y(SCREEN_BOTTOM-60):zoom(0.75):diffuse(color("#FAD0B1")):strokecolor(color("#000000")):maxwidth(400) end;
		OnCommand=function(self) self:diffusealpha(0):sleep(0.3):smooth(0.3):diffusealpha(1) end;
		OffCommand=function(self) self:smooth(0.2):diffusealpha(0) end;
		Text=GAMESTATE:GetCurrentCourse():GetDisplayFullTitle()
	};

	Def.Quad {
		InitCommand=function(self) self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):Center():diffuse(color("#000000")):diffusealpha(0)
		end;
		OffCommand=function(self) self:sleep(1.2):decelerate(0.4):diffusealpha(1):sleep(0.2)
		end;
	};
		
	LoadActor(THEME:GetPathS("ScreenEvaluation", "flags")) .. {
		OnCommand=function(self)
			self:play();
		end;
	};	
};
end;