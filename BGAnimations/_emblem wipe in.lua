return Def.ActorFrame {
	LoadActor(THEME:GetPathG("_logo transition", "base")) .. {
				InitCommand=function(self) self:Center():zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):diffuse(color("#000000")) end;
				OnCommand=function(self) self:diffusealpha(1):sleep(0.4):smooth(0.4):zoom(1.4):diffusealpha(0) end;
	};	
	LoadActor(THEME:GetPathG("_logo transition", "top parts")) .. {
				InitCommand=function(self) self:Center():diffuse(color("#000000")) end;
				OnCommand=function(self) self:diffusealpha(1):sleep(0.05):decelerate(0.1):diffusealpha(0) end;
	};	
	LoadActor(THEME:GetPathG("_logo transition", "side parts")) .. {
				InitCommand=function(self) self:Center():diffuse(color("#000000")) end;
				OnCommand=function(self) self:diffusealpha(1):sleep(0.1):decelerate(0.1):diffusealpha(0) end;
	};	
	LoadActor(THEME:GetPathG("_logo transition", "middle parts")) .. {
				InitCommand=function(self) self:Center():diffuse(color("#000000")) end;
				OnCommand=function(self) self:diffusealpha(1):sleep(0.15):decelerate(0.1):diffusealpha(0) end;
	};	
	LoadActor(THEME:GetPathG("_logo transition", "bottom parts")) .. {
				InitCommand=function(self) self:Center():diffuse(color("#000000")) end;
				OnCommand=function(self) self:diffusealpha(1):sleep(0.2):decelerate(0.1):diffusealpha(0) end;
	};
};