if not GAMESTATE:IsCourseMode() then	
return Def.ActorFrame {
	Def.Quad {
		InitCommand=function(self) self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):Center():diffuse(color("#96E7D7")):diffusebottomedge(color("#17FFD1")) end;
	};
		
	LoadActor("_dia") .. {
		InitCommand=function(self)
			self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):customtexturerect(0,0,SCREEN_WIDTH*4/256,SCREEN_HEIGHT*4/256):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y)
		end;
		OnCommand=function(self)
			self:texcoordvelocity(0.3,0):diffusealpha(0.06)
		end;
	};
		
	Def.Quad {
		InitCommand=function(self) 
			self:zoomto(SCREEN_WIDTH,90):diffuse(color("#0F4A72")):diffusebottomedge(color("#37B3B1"))
			:y(SCREEN_CENTER_Y+90):x(SCREEN_CENTER_X) 
		end;
	};
		
	Def.ActorFrame {
		InitCommand=function(self)
			self:fov(30)
		end;
		LoadActor("_carpet") .. {
			InitCommand=function(self)
				self:zoomto(SCREEN_WIDTH+120,94):vertalign(bottom):customtexturerect(0,0,1,1):x(SCREEN_CENTER_X):y(SCREEN_BOTTOM)
			end;
			OnCommand=function(self)
				self:texcoordvelocity(0.1,0):rotationx(-47)
			end;
		};	
		LoadActor("_hallway") .. {
			InitCommand=function(self)
				self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y-50)
			end;
			OnCommand=function(self)
				self:texcoordvelocity(0.1,0)
			end;
		};		
		LoadActor("_shade") .. {
			InitCommand=function(self)
				self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y-50)
			end;
			OnCommand=function(self)
				self:texcoordvelocity(0.1,0)
			end;
		};	
		LoadActor("_pillar") .. {
			InitCommand=function(self)
				self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y-15)
			end;
			OnCommand=function(self)
				self:texcoordvelocity(0.1,0)
			end;
		};	
	};	
}
else
return Def.ActorFrame {
	LoadActor("_courseunderlay") .. {
		InitCommand=function(self)
			self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):Center()
		end;
	};
	LoadActor("_dia") .. {
		InitCommand=function(self)
			self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):customtexturerect(0,0,SCREEN_WIDTH*4/256,SCREEN_HEIGHT*4/256):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y)
		end;
		OnCommand=function(self)
			self:texcoordvelocity(-0.03,-0.03):diffusealpha(0.11)
		end;
	};	
		
	Def.Quad {
		InitCommand=function(self)
			self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):diffuse(color("0,0,0,0.25")):diffusetopedge(color("0,0,0,0.75"))
		end;
	};	
	
	LoadActor("_rails") .. {
		InitCommand=function(self)
			self:zoomto(SCREEN_WIDTH+100,SCREEN_HEIGHT):customtexturerect(0,0,1,1):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):rotationz(14)
		end;
		OnCommand=function(self)
			self:texcoordvelocity(0.6,0):blend('Add'):diffusealpha(0.11)
		end;
	};
}
end;