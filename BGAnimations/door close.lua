return Def.ActorFrame {
	Def.Quad {
		InitCommand=function(self) self:Center():zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):diffuse(Color.Black) end;
		OnCommand=function(self) self:diffusealpha(0):sleep(0.5):decelerate(0.5):diffusealpha(0.5) end;
	};		
	LoadActor(THEME:GetPathG("_door", "bottom")) .. {
		InitCommand=function(self) self:horizalign(center):vertalign(top):x(SCREEN_CENTER_X):y(SCREEN_BOTTOM) end;
		OnCommand=function(self) self:decelerate(0.5):y(SCREEN_CENTER_Y-8):sleep(0.4) end;
	};	
	LoadActor(THEME:GetPathG("_door", "top")) .. {
		InitCommand=function(self) self:horizalign(center):vertalign(bottom):x(SCREEN_CENTER_X):y(SCREEN_TOP) end;
		OnCommand=function(self) self:sleep(0.5):decelerate(0.5):y(SCREEN_CENTER_Y+100-10):sleep(0.4) end;
	};
	LoadActor(THEME:GetPathS("ScreenTransition", "close door")) .. {
		StartTransitioningCommand=function(self)
			self:play();
		end;
	};
collectgarbage();
};