return Def.ActorFrame {

	Def.Sprite {
		OnCommand=function(self) 
			self:queuecommand("Set")
		end;		
		SetCommand=function(self) 
			if (STATSMAN:GetCurStageStats():OnePassed() == true) then
				self:Load(THEME:GetPathB("","ScreenEvaluation background/_clearbg"))
			elseif (STATSMAN:GetCurStageStats():OnePassed() == false) then
				self:Load(THEME:GetPathB("","ScreenEvaluation background/_failbg"))
			else
				self:Load(THEME:GetPathG("","_blank"))
			end;
			self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):Center()
		end;
	};	

	LoadActor("_gradebg") .. {
		InitCommand=function(self) self:Center() end;
		OnCommand=function(self) 
			self:diffusecolor(color("#FFFFFF")):smooth(0.2):diffusealpha(1)
		end;
	};

};