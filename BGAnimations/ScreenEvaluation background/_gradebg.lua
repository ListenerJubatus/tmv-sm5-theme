local Random6 = math.random(1,6);
local Random7 = math.random(1,7);
local Random9 = math.random(1,9);
local gradeInput = STATSMAN:GetBestGrade();
local gradeInt = Grade:Reverse()[gradeInput];

-- Had to divide them into ranges due to expanded grades. However, this also means that the AAA-tier BGs of previous TM themes are slightly easier to attain 
-- i,e AA+/ITG three-star rather than requiring a PFC
return Def.ActorFrame {
	Def.Sprite {
		OnCommand=function(self) 
			self:queuecommand("Set")
		end;		
		SetCommand=function(self) 
			if gradeInt >= 6 and gradeInt <= 9 then
				self:Load(THEME:GetPathB("ScreenEvaluation background/_lowtier/drop",tostring(Random6)))
			elseif gradeInt >= 3 and gradeInt <= 5 then
				self:Load(THEME:GetPathB("ScreenEvaluation background/_midtier/drop",tostring(Random9)))
			elseif gradeInt >= 0 and gradeInt <= 2 then
				self:Load(THEME:GetPathB("ScreenEvaluation background/_toptier/drop",tostring(Random7)))
			else
				self:Load(THEME:GetPathG("","_blank"))
			end;
			self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT) 
		end;
	};
	-- Easter eggs
	Def.Sprite {
		OnCommand=function(self) 
			self:queuecommand("Set")
		end;		
		SetCommand=function(self) 
			if GAMESTATE:GetCurrentSong():GetDisplayFullTitle() == "Gokai! Goka!? Phantom Thief!" then
				self:Load(THEME:GetPathB("ScreenEvaluation background/_toptier/drop","3"))
			elseif GAMESTATE:GetCurrentSong():GetDisplayFullTitle() == "Last Surprise" then
				self:Load(THEME:GetPathB("ScreenEvaluation background/_toptier/drop","3"))
			elseif GAMESTATE:GetCurrentSong():GetDisplayFullTitle() == "Godspeed" then
				self:Load(THEME:GetPathB("","ScreenEvaluation background/_godtier/_godspeed"))
			elseif GAMESTATE:GetCurrentSong():GetDisplayFullTitle() == "Unhappy Refrain" then
				self:Load(THEME:GetPathB("","ScreenEvaluation background/_godtier/_refrain"))
			else
				self:Load(THEME:GetPathG("","_blank"))
			end;
			self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT) 
		end;
	};
}