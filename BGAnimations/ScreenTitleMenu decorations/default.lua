InitUserPrefs();
local eggs = 0
return Def.ActorFrame {

	Def.ActorFrame {
		OnCommand=function(self)
			if not FILEMAN:DoesFileExist("Save/ThemePrefs.ini") then
				Trace("ThemePrefs doesn't exist; creating file")
				ThemePrefs.ForceSave()
			end

			ThemePrefs.Save()
		end;
	};


	Def.ActorFrame {
		LoadActor("_metalwork") .. {
			InitCommand=function(self) self:horizalign(right):xy(SCREEN_RIGHT,SCREEN_CENTER_Y+103):zoom(0.75) end;
			OnCommand=function(self) self:queuecommand("Transition") end;
			TransitionMessageCommand=function(self) self:decelerate(3.2):y(SCREEN_CENTER_Y-70) end;
			CodeMessageCommand = function(self, params)
				if params.Name=="EffectedBy" then
					if eggs == 0 then
						self:bouncebegin(0.2):addy(10):bounceend(1):addy(-510)
					end;
				end;
			end;
		};		
		Def.ActorFrame {
		OnCommand=function(self) 
			self:fov(20)
		end;
		LoadActor("_logo") .. {
			InitCommand=function(self) self:horizalign(center):vertalign(top):xy(SCREEN_CENTER_X,SCREEN_CENTER_Y+157-135):zoom(0.75) end;
			OnCommand=function(self) 
				self:wag():effectmagnitude(13,0,0):effectperiod(9)
				self:queuecommand("Transition")
			end;
			TransitionMessageCommand=function(self) self:decelerate(3.2):y(SCREEN_CENTER_Y-17-135) end;
			CodeMessageCommand = function(self, params)
				if params.Name=="EffectedBy" then
					if eggs == 0 then
						self:bouncebegin(0.2):addy(10):bounceend(1):addy(-510)
					end;
				end;
			end;
		};
		};
		LoadActor(THEME:GetPathS("ScreenTransition", "easter egg")) .. {
			CodeMessageCommand = function(self, params)
				if params.Name=="EffectedBy" then
					if eggs == 0 then
						self:play();
					end;
				end;
			end;
		};		
		
		LoadActor("_vividwave") .. {
			InitCommand=function(self) self:horizalign(right):xy(SCREEN_RIGHT,SCREEN_CENTER_Y-70):zoom(0.75):diffusealpha(0) end;
			OnCommand=function(self) self:queuecommand("Transition") end;
			TransitionMessageCommand=function(self) self:decelerate(3.2):y(SCREEN_CENTER_Y-70) end;
			CodeMessageCommand = function(self, params)
				if params.Name=="EffectedBy" then
					if eggs == 0 then
						self:sleep(1.4):addy(-510):diffusealpha(1):bouncebegin(1):addy(520):bounceend(0.8):addy(-20)
						eggs = 1
					end;
				end;
			end;
		};	
		LoadActor("_frame") .. {
			InitCommand=function(self) self:horizalign(right):xy(SCREEN_RIGHT,SCREEN_CENTER_Y+103):zoom(0.75) end;
			OnCommand=function(self) self:queuecommand("Transition") end;
			TransitionMessageCommand=function(self) self:decelerate(3.2):y(SCREEN_CENTER_Y-70) end;
		};	
		LoadActor("_sidewood") .. {
			InitCommand=function(self) self:horizalign(right):xy(SCREEN_RIGHT,SCREEN_CENTER_Y+103):zoom(0.75) end;
			OnCommand=function(self) self:queuecommand("Transition") end;
			TransitionMessageCommand=function(self) self:decelerate(3.2):y(SCREEN_CENTER_Y-70) end;
		};
	};

	Def.Quad {
		InitCommand=function(self) 
			self:zoomto(SCREEN_WIDTH,120):vertalign(bottom):xy(SCREEN_CENTER_X,SCREEN_BOTTOM):diffuse(color("#000000")):fadetop(1):diffusealpha(0.75) 
		end;
	};

	Def.BitmapText { 
		Font= "Common Normal",
		Text=SONGMAN:GetNumSongs() .. " songs in " .. SONGMAN:GetNumSongGroups() .. " groups",
		InitCommand=function(self) 
			self:shadowlength(1):zoom(0.75):xy(16,24):diffuse(color("#E5C551")):diffusetopedge(color("#ECDEAB")):strokecolor(Color.Black):horizalign(left) 
		end;			
		OnCommand=function(self)
			self:diffusealpha(0):smooth(0.4):diffusealpha(1)
		end;
		OffCommand=function(self) self:smooth(0.4):diffusealpha(0) end;
	};

	Def.BitmapText { 
		Font= "Common Normal",
		Text=SONGMAN:GetNumCourses() .. " courses",
		InitCommand=function(self) 
			self:shadowlength(1):zoom(0.75):xy(16,44):diffuse(color("#E5C551")):diffusetopedge(color("#ECDEAB")):strokecolor(Color.Black):horizalign(left) 
		end;			
		OnCommand=function(self)
			self:diffusealpha(0):smooth(0.4):diffusealpha(1)
		end;
		OffCommand=function(self) self:smooth(0.4):diffusealpha(0) end;
	};
		
	Def.BitmapText { 
		Font= "Common Normal",
		Text= string.format("StepMania %s", ProductVersion()), 
		InitCommand=function(self) 
			self:shadowlength(1):zoom(0.75):x(SCREEN_RIGHT-16):y(24)
			self:diffuse(color("#E5C551")):diffusetopedge(color("#ECDEAB")):strokecolor(Color.Black):horizalign(right) 
		end;
		OnCommand=function(self)
			self:diffusealpha(0):smooth(0.4):diffusealpha(1)
		end;
		OffCommand=function(self) self:smooth(0.4):diffusealpha(0) end;
	};

	Def.BitmapText { 
		Font= "Common Normal",
		Text= VersionDate(),
		InitCommand=function(self) 
			self:shadowlength(1):zoom(0.5):x(SCREEN_RIGHT-16):y(40)
			self:diffuse(color("#E5C551")):diffusetopedge(color("#ECDEAB")):strokecolor(Color.Black):horizalign(right) 
		end;
		OnCommand=function(self)
			self:diffusealpha(0):smooth(0.4):diffusealpha(1)
		end;
		OffCommand=function(self) self:smooth(0.4):diffusealpha(0) end;
	};

	Def.BitmapText { 
		Font= "Common Normal",
		Text= "v1.0.1",
		InitCommand=function(self) 
			self:horizalign(center):zoom(0.6):shadowlength(1):x(SCREEN_CENTER_X):y(SCREEN_BOTTOM-14)
			self:diffuse(color("#E5C551")):diffusetopedge(color("#ECDEAB")):strokecolor(Color.Black)
		end;
		OnCommand=function(self)
			self:diffusealpha(0):smooth(0.4):diffusealpha(1)
		end;
		OffCommand=function(self) self:smooth(0.4):diffusealpha(0) end;
	};
		
	StandardDecorationFromFileOptional("NetworkStatus","NetworkStatus");

};