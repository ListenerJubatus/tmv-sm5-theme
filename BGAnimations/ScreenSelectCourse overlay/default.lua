local t = Def.ActorFrame {};

-- BG for list
t[#t+1] = LoadActor("_pane") .. {
		InitCommand=function(self)
			self:horizalign(left):x(SCREEN_RIGHT):y(SCREEN_CENTER_Y)
		end;
		OnCommand=function(self)
			self:decelerate(0.7):addx(-640)
		end;		
		OffCommand=function(self)
			self:decelerate(0.6):addx(640)
		end;
};

-- Course title
t[#t+1] = LoadFont("Common Normal") .. {
		InitCommand=function(self)
			self:x(SCREEN_CENTER_X+46):y(SCREEN_CENTER_Y-106+8):maxwidth(360):horizalign(left):strokecolor(color("#000000"))
		end;
		OnCommand=function(self)
			self:queuecommand("Set"):addx(640):decelerate(0.7):addx(-640)
		end;
		OffCommand=function(self)
			self:decelerate(0.6):addx(640)
		end;		
		CurrentSongChangedMessageCommand=function(self) self:queuecommand("Set") end;
		CurrentCourseChangedMessageCommand=function(self) self:queuecommand("Set") end;
		ChangedLanguageDisplayMessageCommand=function(self) self:queuecommand("Set") end;
		SetCommand=function(self) 
		local course = GAMESTATE:GetCurrentCourse(); 
			if course then
				self:settext(course:GetDisplayFullTitle())
			else
				self:settext("");
			end 
		end; 
	};

-- Course type	
	t[#t+1] = LoadFont("Common Normal") .. { 
		InitCommand=function(self)
			self:diffuse(color("#E99E3C")):strokecolor(color("#000000")):horizalign(left)
			self:x(SCREEN_CENTER_X+46):y(SCREEN_CENTER_Y-120):maxwidth(450):zoom(0.5)
		end;
		OnCommand=function(self)
			self:queuecommand("Set"):addx(640):decelerate(0.7):addx(-640)
		end;
		OffCommand=function(self)
			self:decelerate(0.6):addx(640)
		end;
		CurrentSongChangedMessageCommand=function(self) self:queuecommand("Set") end;
		CurrentCourseChangedMessageCommand=function(self) self:queuecommand("Set") end;
		ChangedLanguageDisplayMessageCommand=function(self) self:queuecommand("Set") end;
		SetCommand=function(self) 
		local course = GAMESTATE:GetCurrentCourse();
		   if course then
				self:settext(CourseTypeToLocalizedString(course:GetCourseType())); 
			else
				self:settext("");
		   end 
		end; 
	};
	t[#t+1] = Def.ActorFrame{
	OnCommand=function(self) self:fov(30) end;
		Def.Sprite {
		name="SongJacket";
		InitCommand=function(self) self:x(SCREEN_CENTER_X+360):y(SCREEN_CENTER_Y+10):rotationy(50) end;
		OnCommand=function(self) self:diffusealpha(0):rotationy(80):sleep(0.6):decelerate(0.4):rotationy(50):diffusealpha(1) end;		
		OffCommand=function(self) self:decelerate(0.6):addx(640) end;	
		CurrentCourseChangedMessageCommand=function(self) self:playcommand("Set") end;
		SetCommand=function(self)
			local course = GAMESTATE:GetCurrentCourse();
			if course then
				if course:HasBanner() == true then
					self:finishtweening()
					self:Load(course:GetBannerPath())
					self:scaletoclipped(128,128):diffusealpha(0):rotationy(70):linear(0.12):rotationy(50):diffusealpha(1)
				else
					self:finishtweening()
					self:Load(THEME:GetPathG("Common fallback", "course"))
					self:scaletoclipped(128,128):diffusealpha(0):rotationy(70):linear(0.12):rotationy(50):diffusealpha(1)
				end;
			else
				self:Load(THEME:GetPathG("Common fallback", "course"))
			end;
		end;
		};
	};
	t[#t+1] = StandardDecorationFromFileOptional("CourseContentsList","CourseContentsList");

-- # of songs
	t[#t+1] = LoadFont("Common Normal") .. {
		InitCommand=function(self)
			self:x(SCREEN_CENTER_X+60):y(SCREEN_CENTER_Y+108):horizalign(left):strokecolor(color("#000000")):zoom(0.75):maxwidth(120)
			self:settext("# OF SONGS")
		end;
		OnCommand=function(self)
			self:addx(640):decelerate(0.7):addx(-640)
		end;
		OffCommand=function(self)
			self:decelerate(0.6):addx(640)
		end;	
	};
	
	t[#t+1] = LoadFont("Common Normal") .. { 
		InitCommand=function(self)
			self:x(SCREEN_CENTER_X+60+120):y(SCREEN_CENTER_Y+108):horizalign(left):strokecolor(color("#000000")):zoom(0.75)
		end;
		OnCommand=function(self)
			self:queuecommand("Set"):addx(640):decelerate(0.7):addx(-640)
		end;
		OffCommand=function(self)
			self:decelerate(0.6):addx(640)
		end;
		CurrentSongChangedMessageCommand=function(self) self:queuecommand("Set") end;
		CurrentCourseChangedMessageCommand=function(self) self:queuecommand("Set") end;
		ChangedLanguageDisplayMessageCommand=function(self) self:queuecommand("Set") end;
		SetCommand=function(self) 
		   local course = GAMESTATE:GetCurrentCourse(); 
		   if course then
				self:settext(course:GetEstimatedNumStages()); 
				self:queuecommand("Refresh");
			else
				self:settext("");
				self:queuecommand("Refresh"); 	
		   end
	  end;
	};

-- Course sort mode
t[#t+1] = LoadFont("_decadentafrax 24px") .. {
	InitCommand=function(self)
		self:x(SCREEN_CENTER_X):y(SCREEN_TOP+30):zoom(1):maxwidth(200):diffuse(color("#FFFFFF")):strokecolor(color("#000000"))
	end;
	OnCommand=function(self) self:diffusealpha(0):sleep(1):zoomx(0.9):decelerate(0.3):zoomx(1.1):diffusealpha(1) end;
	OffCommand=function(self) self:decelerate(0.5):addy(-120) end;
    SortOrderChangedMessageCommand=function(self) self:playcommand("Set") end;
	ChangedLanguageDisplayMessageCommand=function(self) self:playcommand("Set") end;
	SetCommand=function(self)
	   local sortorder = GAMESTATE:GetSortOrder();
	   if sortorder then
			self:settext(SortOrderToLocalizedString(sortorder));
			self:playcommand("Refresh");
		else
			self:settext("");
			self:playcommand("Refresh");
	   end
	end;
};


t[#t+1] = StandardDecorationFromFileOptional("DifficultyList","DifficultyList");
t[#t+1] = StandardDecorationFromFileOptional("BPMDisplay","BPMDisplay");
t[#t+1] = StandardDecorationFromFileOptional("SongTime","SongTime") .. {
	SetCommand=function(self)
		local curSelection = nil;
		local length = 0.0;
		if GAMESTATE:IsCourseMode() then
			curSelection = GAMESTATE:GetCurrentCourse();
			self:playcommand("Reset");
			if curSelection then
				local trail = GAMESTATE:GetCurrentTrail(GAMESTATE:GetMasterPlayerNumber());
				if trail then
					length = TrailUtil.GetTotalSeconds(trail);
				else
					length = 0.0;
				end;
			else
				length = 0.0;
			end;
		else
			curSelection = GAMESTATE:GetCurrentSong();
			self:playcommand("Reset");
			if curSelection then
				length = curSelection:MusicLengthSeconds();
				if curSelection:IsLong() then
					self:playcommand("Long");
				elseif curSelection:IsMarathon() then
					self:playcommand("Marathon");
				else
					self:playcommand("Reset");
				end
			else
				length = 0.0;
				self:playcommand("Reset");
			end;
		end;
		self:settext( SecondsToMSS(length) );
	end;
	CurrentSongChangedMessageCommand=function(self) self:playcommand("Set") end;
	CurrentCourseChangedMessageCommand=function(self) self:playcommand("Set") end;
	CurrentTrailP1ChangedMessageCommand=function(self) self:playcommand("Set") end;
	CurrentTrailP2ChangedMessageCommand=function(self) self:playcommand("Set") end;
};

for pn in ivalues(PlayerNumber) do
local paneloffset = string.find(pn, "P1") and SCREEN_CENTER_X-200 or SCREEN_CENTER_X+200;
local animdistance = string.find(pn, "P1") and 640 or -640;
local panel_height = SCREEN_BOTTOM-60
	t[#t+1] = Def.ActorFrame {
		InitCommand=function(self)
			self:horizalign(center):x(paneloffset):y(panel_height)
		end;
		OnCommand=function(self)
			self:diffusealpha(0):sleep(0.6):decelerate(0.3):diffusealpha(1)
		end;		
		OffCommand=function(self)
			self:decelerate(0.6):diffusealpha(0)
		end;	
		LoadActor("_playertag") .. {
			OnCommand=function(self)
				self:diffuse(ColorLightTone(PlayerColor(pn)))
			end;		
		};
		LoadActor("_difficultytag") .. {
			OnCommand=function(self)
				self:diffuse(color("#846B98"))
			end;
			["CurrentSteps"..ToEnumShortString(pn).."ChangedMessageCommand"]=function(self) self:queuecommand("Set") end; 
			SetCommand=function(self)
				local song = GAMESTATE:GetCurrentCourse()
				local stepsData = GAMESTATE:GetCurrentTrail(pn)
				if song then 
					if stepsData ~= nil then
						local cd = GetCustomDifficulty(stepsData:GetStepsType(), stepsData:GetDifficulty(), song:GetCourseType());
						self:diffuse(CustomDifficultyToColor(cd))
					end
				end
			end;			
		};
	};
	t[#t+1] = LoadFont("Common Normal") .. {
		InitCommand=function(self)
		self:zoom(1):y(panel_height):x(paneloffset):maxwidth(180):uppercase(true):horizalign(center):strokecolor(color("#000000"))
		end;
		OnCommand=function(self)
			self:diffusealpha(0):sleep(0.6):decelerate(0.3):diffusealpha(1)
		end;		
		OffCommand=function(self)
			self:decelerate(0.6):diffusealpha(0)
		end;		  
		["CurrentSteps"..ToEnumShortString(pn).."ChangedMessageCommand"]=function(self) self:queuecommand("Set") end; 
		PlayerJoinedMessageCommand=function(self,param)
			if param.Player == pn then
				self:queuecommand("Set"):diffusealpha(0):smooth(0.3):diffusealpha(1) 
			end;
		end;
		ChangedLanguageDisplayMessageCommand=function(self) self:queuecommand("Set") end;
		  SetCommand=function(self)
			local song = GAMESTATE:GetCurrentCourse()
			local stepsData = GAMESTATE:GetCurrentTrail(pn)
			if song then 
				if stepsData ~= nil then
					local st = stepsData:GetStepsType();
					local diff = stepsData:GetDifficulty();
					local courseType = GAMESTATE:IsCourseMode() and song:GetCourseType() or nil;
					local cd = GetCustomDifficulty(st, diff, courseType);
					self:settext(THEME:GetString("CustomDifficulty",ToEnumShortString(diff)))
					self:diffuse(color("#FFFFFF")):diffusetopedge(ColorLightTone(CustomDifficultyToColor(cd)))
				else
					self:settext("")
				end
			else
				self:settext("")
			end
		  end
		};
end;
return t