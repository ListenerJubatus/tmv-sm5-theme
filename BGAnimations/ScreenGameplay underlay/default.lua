return Def.ActorFrame {

	LoadActor("ScreenFilter");

	LoadActor("_dead");	

	LoadActor("_splash");
	
	Def.Quad {
			InitCommand=function(self)
				self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):diffuse(color("#000000"))
			end;
			OnCommand=function(self)
				self:decelerate(0.4):diffusealpha(0)
			end;
	};
	
	--Stage readout
	Def.ActorFrame {
		InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_TOP-42) end;
		OnCommand=function(self)
			self:sleep(0.8):decelerate(0.3):addy(73):bouncebegin(0.2):addy(-3):bounceend(0.3):addy(3)
		end;
		OffCommand=function(self)
			self:sleep(0.6):decelerate(0.5):addy(-73)
		end;
		LoadActor(THEME:GetPathG("ScreenGameplay", "top frame"));
		LoadFont("Common Normal") .. {
			InitCommand=function(self) self:horizalign(center):addy(-6) end;
			CurrentSongChangedMessageCommand=function(self) self:playcommand("Set") end;
			CurrentCourseChangedMessageCommand=function(self) self:playcommand("Set") end;
			CurrentStepsP1ChangedMessageCommand=function(self) self:playcommand("Set") end;
			CurrentStepsP2ChangedMessageCommand=function(self) self:playcommand("Set") end;
			CurrentTraiP1ChangedMessageCommand=function(self) self:playcommand("Set") end;
			CurrentTraiP2ChangedMessageCommand=function(self) self:playcommand("Set") end;
			SetCommand=function(self)
				local curScreen = Var "LoadingScreen";
				local curStageIndex = GAMESTATE:GetCurrentStageIndex() + 1;
				local playMode = GAMESTATE:GetPlayMode();
				local curStage = GAMESTATE:GetCurrentStage();
				if GAMESTATE:IsCourseMode() then
					local stats = STATSMAN:GetCurStageStats()
					if not stats then
						return
					end
					local mpStats = stats:GetPlayerStageStats( GAMESTATE:GetMasterPlayerNumber() )
					local songsPlayed = mpStats:GetSongsPassed() + 1
					self:settextf("%i / %i", songsPlayed, GAMESTATE:GetCurrentCourse():GetEstimatedNumStages());
				else
					if GAMESTATE:IsEventMode() then
						self:settextf("FREE");
					else
						self:settextf("%s", ToEnumShortString(curStage));
					end
				end;
				self:zoom(0.75);
				self:strokecolor(ColorDarkTone(StageToColor(curStage)));
			end;
		};
	};
};