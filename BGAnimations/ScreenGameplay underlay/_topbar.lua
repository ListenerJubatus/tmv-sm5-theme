local gauntlet_colors = {
	"#9153E0", -- Hephaestus	1
	"#3CA4E2", -- Euphrosyne	2
	"#28AD91", -- Eir			3
	"#CBC5AE", -- Radha			4
	"#FFAB58", -- Kohara		5
	"#AD000C", -- Hekau			6
	"#0483C7", -- Special for How Far We've Come			7
	"#FFFFFF", -- NaN			8
};

return Def.ActorFrame {
	LoadActor("_frame") .. {
		OnCommand=function(self) self:playcommand("Set") end;
		CurrentSongChangedMessageCommand=function(self) self:playcommand("Set") end;
		CurrentCourseChangedMessageCommand=function(self) self:playcommand("Set") end;
		CurrentStepsP1ChangedMessageCommand=function(self) self:playcommand("Set") end;
		CurrentStepsP2ChangedMessageCommand=function(self) self:playcommand("Set") end;
		CurrentTraiP1ChangedMessageCommand=function(self) self:playcommand("Set") end;
		CurrentTraiP2ChangedMessageCommand=function(self) self:playcommand("Set") end;
		SetCommand=function(self)
		local gauntlet_colors = {
			"#9153E0", -- Hephaestus	1
			"#3CA4E2", -- Euphrosyne	2
			"#28AD91", -- Eir			3
			"#CBC5AE", -- Radha			4
			"#FFAB58", -- Kohara		5
			"#AD000C", -- Hekau			6
			"#000000", -- NaN			7
		};
		local curScreen = Var "LoadingScreen";
		local playMode = GAMESTATE:GetPlayMode();
		local curStage = GAMESTATE:GetCurrentStage();
		if GAMESTATE:IsCourseMode() then
				self:diffuse(StageToColor(curStage))
			else
				local song = GAMESTATE:GetCurrentSong():GetDisplayFullTitle()
				if song == "Faster Than a Lightning" then
					self:diffuse(color(gauntlet_colors[1]))
				elseif song == "Art of Manifestation" then
					self:diffuse(color(gauntlet_colors[2]))
				elseif song == "The Outdoors (アイスベア's NATUREMiX)" then
					self:diffuse(color(gauntlet_colors[3]))
				elseif song == "APPLEBUCKIN'" then
					self:diffuse(color(gauntlet_colors[4]))
				elseif song == "Cerebral Meltdown" then
					self:diffuse(color(gauntlet_colors[5]))
				elseif song == "Trixie the Pony Troll (Brilliant Venture & One Track Mind Bootleg Mix)" then
					self:diffuse(color(gauntlet_colors[6]))
				else
					self:diffuse(StageToColor(curStage))
				end
			end;				
		self:diffusealpha(0.95)
	}
	LoadActor(THEME:GetPathG("ScreenGameplay", "top decor"));
}