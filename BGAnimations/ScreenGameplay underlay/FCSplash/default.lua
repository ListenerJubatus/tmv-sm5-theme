--FullCombo base from moonlight by AJ

local pn = ...;
assert(pn);

local t = Def.ActorFrame{};

local IsUsingSoloSingles = PREFSMAN:GetPreference('Center1Player');
local NumPlayers = GAMESTATE:GetNumPlayersEnabled();
local NumSides = GAMESTATE:GetNumSidesJoined();

local pStats = STATSMAN:GetCurStageStats():GetPlayerStageStats(pn);

local function GetPosition(pn)
	if IsUsingSoloSingles and NumPlayers == 1 and NumSides == 1 then return SCREEN_CENTER_X; end;

	return THEME:GetMetric("ScreenGameplay","Player".. ToEnumShortString(pn) .. "MiscX");

end;

--Marvelous FullCombo
t[#t+1] = LoadActor("Text_MarvelousFullCombo")..{
	InitCommand=function(self) self:diffusealpha(0):x(GetPosition(pn)):vertalign(bottom):y(SCREEN_TOP) end;
	OffCommand=function(self)
		local fct = STATSMAN:GetCurStageStats():GetPlayerStageStats(pn);
		if fct:FullComboOfScore('TapNoteScore_W1') == true then
			self:diffusealpha(1):decelerate(0.3):addy(340):bouncebegin(0.2):addy(-3):bounceend(0.3):addy(3):sleep(1.1):decelerate(0.5):addy(-360)
		elseif fct:FullComboOfScore('TapNoteScore_W2') == true then
			self:visible(false);
		elseif fct:FullComboOfScore('TapNoteScore_W3') == true then
			self:visible(false);
		else
			self:visible(false);
		end;
	end;	
};

--Perfect FullCombo
t[#t+1] = LoadActor("Text_PerfectFullCombo")..{
	InitCommand=function(self) self:diffusealpha(0):x(GetPosition(pn)):vertalign(bottom):y(SCREEN_TOP) end;
	OffCommand=function(self)
		local fct = STATSMAN:GetCurStageStats():GetPlayerStageStats(pn);
		if fct:FullComboOfScore('TapNoteScore_W1') == true then
			self:visible(false);
		elseif fct:FullComboOfScore('TapNoteScore_W2') == true then
			self:diffusealpha(1):decelerate(0.3):addy(340):bouncebegin(0.2):addy(-3):bounceend(0.3):addy(3):sleep(1.1):decelerate(0.5):addy(-360)
		elseif fct:FullComboOfScore('TapNoteScore_W3') == true then
			self:visible(false);
		else
			self:visible(false);
		end;
	end;	
};

--FullCombo
t[#t+1] = LoadActor("Text_FullCombo")..{
	InitCommand=function(self) self:diffusealpha(0):x(GetPosition(pn)):vertalign(bottom):y(SCREEN_TOP) end;
	OffCommand=function(self)
		local fct = STATSMAN:GetCurStageStats():GetPlayerStageStats(pn);
		if fct:FullComboOfScore('TapNoteScore_W1') == true then
			self:visible(false);
		elseif fct:FullComboOfScore('TapNoteScore_W2') == true then
			self:visible(false);
		elseif fct:FullComboOfScore('TapNoteScore_W3') == true then
			self:diffusealpha(1):decelerate(0.3):addy(340):bouncebegin(0.2):addy(-3):bounceend(0.3):addy(3):sleep(1.1):decelerate(0.5):addy(-360)
		else
			self:visible(false);
		end;
	end;	
};
	

t[#t+1] = LoadActor("_splash") .. {
	OffCommand=function(self)
	local fct = STATSMAN:GetCurStageStats():GetPlayerStageStats(pn);
		if fct:FullCombo() then
			self:play();
		end;
	end;
};	

return t;