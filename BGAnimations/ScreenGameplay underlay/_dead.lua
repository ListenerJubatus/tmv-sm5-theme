local prevhealthP1 = "HealthState_Alive"
local prevhealthP2 = "HealthState_Alive"


return Def.ActorFrame{
	Def.ActorFrame{
		Name="DeadP1";
		Def.ActorFrame{
			BeginCommand=function(self)
				local style = GAMESTATE:GetCurrentStyle()
				local styleType = style:GetStyleType()
				self:visible( styleType ~= "StyleType_OnePlayerTwoSides" and styleType ~= "StyleType_TwoPlayersSharedSides" )
			end;
			HealthStateChangedMessageCommand=function(self, param)
				if param.PlayerNumber == PLAYER_1 then
					if param.HealthState == "HealthState_Dead" then
						self:RunCommandsOnChildren(function(self) self:playcommand("Show") end)
						prevHealthP1 = "HealthState_Dead"
					else
						if param.HealthState == "HealthState_Danger" and prevHealthP1 == "HealthState_Dead" then
							self:RunCommandsOnChildren(function(self) self:playcommand("Hide") end)
							prevHealthP1 = "HealthState_Danger"
						end
					end
				end
			end;
			-- SFX
			LoadActor("_blazed") .. {
				ShowCommand=function(self) self:play() end;
			};
			-- Player 1 Center
			LoadActor("_widechar")..{
				Name="DeadP1";
				Condition=Center1Player() == true;
				InitCommand=function(self) self:stretchto(SCREEN_LEFT,SCREEN_TOP,SCREEN_RIGHT,SCREEN_BOTTOM+50)
							:diffusealpha(0.6):croptop(1):player(PLAYER_1)
				end;
				ShowCommand=function(self) self:croptop(1):diffusealpha(0.6):finishtweening():linear(0.6):croptop(0) end;
				HideCommand=function(self)
					self:finishtweening():linear(2):diffusealpha(0)
				end;
			};
			LoadActor("_fire")..{
				Name="DeadP1";
				Condition=Center1Player() == true;
				InitCommand=function(self)
					self:x(SCREEN_CENTER_X):y(SCREEN_BOTTOM+100):zoomtowidth(SCREEN_WIDTH):zoomy(3.5)
					self:fadebottom(0.1):diffusealpha(0):blend("add"):player(PLAYER_1)
				end;
				ShowCommand=function(self)
					self:finishtweening():diffusealpha(1):linear(0.6):addy(-630):linear(0.1):diffusealpha(0) end;
				HideCommand=function(self)
					self:finishtweening():y(SCREEN_BOTTOM+100)
				end;
			};
			-- Player 1 Side
			LoadActor("_charred")..{
				Name="DeadP1";
				Condition=Center1Player() == false;
				InitCommand=function(self)
					self:draworder(100):faderight(0.1):stretchto(SCREEN_LEFT,SCREEN_TOP,SCREEN_CENTER_X,SCREEN_BOTTOM+170)
					self:diffusealpha(0.6):croptop(1):player(PLAYER_1);
				end;
				ShowCommand=function(self)
					self:croptop(1):diffusealpha(0.6):finishtweening():linear(0.6):croptop(0) end;
				HideCommand=function(self)
					self:finishtweening():linear(2):diffusealpha(0) end;
			};
			LoadActor("_fire")..{
				Name="DeadP1";
				Condition=Center1Player() == false;
				InitCommand=function(self)
					self:y(SCREEN_BOTTOM+230):draworder(101):stretchto(SCREEN_LEFT,SCREEN_BOTTOM-250,SCREEN_CENTER_X,SCREEN_BOTTOM):y(SCREEN_BOTTOM+230)
					:fadebottom(0.1):faderight(0.1):diffusealpha(0):blend("add"):player(PLAYER_1)
				end;			
				ShowCommand=function(self)
					self:finishtweening():diffusealpha(1):linear(0.6):addy(-730):linear(0.1):diffusealpha(0) end;
				HideCommand=function(self)
					self:finishtweening():y(SCREEN_BOTTOM+250) end;
			};
		};
	};


	Def.ActorFrame{
		Name="DeadP2";
		Def.ActorFrame{
			BeginCommand=function(self)
				local style = GAMESTATE:GetCurrentStyle()
				local styleType = style:GetStyleType()
				self:visible( styleType ~= "StyleType_OnePlayerTwoSides" and styleType ~= "StyleType_TwoPlayersSharedSides" )
			end;
			HealthStateChangedMessageCommand=function(self, param)
				if param.PlayerNumber == PLAYER_2 then
					if param.HealthState == "HealthState_Dead" then
						self:RunCommandsOnChildren(function(self) self:playcommand("Show") end)
						prevHealthP2 = "HealthState_Dead"
					else
						if param.HealthState == "HealthState_Danger" and prevHealthP2 == "HealthState_Dead" then
							self:RunCommandsOnChildren(function(self) self:playcommand("Hide") end)
							prevHealthP2 = "HealthState_Danger"
						end
					end
				end
			end;
			-- SFX
			LoadActor("_blazed") .. {
				ShowCommand=function(self) self:play() end;
			};
			-- Player 2 Center
			LoadActor("_widechar")..{
				Name="DeadP2";
				Condition=Center1Player() == true;
				InitCommand=function(self) self:stretchto(SCREEN_LEFT,SCREEN_TOP,SCREEN_RIGHT,SCREEN_BOTTOM+50)
							:diffusealpha(0.6):croptop(1):player(PLAYER_2)
				end;
				ShowCommand=function(self) self:croptop(1):diffusealpha(0.6):finishtweening():linear(0.6):croptop(0) end;
				HideCommand=function(self)
					self:finishtweening():linear(2):diffusealpha(0)
				end;
			};
			LoadActor("_fire")..{
				Name="DeadP2";
				Condition=Center1Player() == true;
				InitCommand=function(self)
					self:x(SCREEN_CENTER_X):y(SCREEN_BOTTOM+100):zoomtowidth(SCREEN_WIDTH):zoomy(3.5)
					self:fadebottom(0.1):diffusealpha(0):blend("add"):player(PLAYER_2)
				end;
				ShowCommand=function(self)
					self:finishtweening():diffusealpha(1):linear(0.6):addy(-630):linear(0.1):diffusealpha(0) end;
				HideCommand=function(self)
					self:finishtweening():y(SCREEN_BOTTOM+100)
				end;
			};
			-- Player 2 Side
			LoadActor("_charred")..{
				Name="DeadP2";
				Condition=Center1Player() == false;
				InitCommand=function(self)
					self:draworder(100):fadeleft(0.1):stretchto(SCREEN_CENTER_X,SCREEN_TOP,SCREEN_RIGHT,SCREEN_BOTTOM+170)
					self:diffusealpha(0.6):croptop(1):player(PLAYER_2);
				end;
				ShowCommand=function(self)
					self:croptop(1):diffusealpha(0.6):finishtweening():linear(0.6):croptop(0) end;
				HideCommand=function(self)
					self:finishtweening():linear(2):diffusealpha(0) end;
			};
			LoadActor("_fire")..{
				Name="DeadP2";
				Condition=Center1Player() == false;
				InitCommand=function(self)
					self:y(SCREEN_BOTTOM+230):draworder(101):stretchto(SCREEN_CENTER_X,SCREEN_BOTTOM-250,SCREEN_RIGHT,SCREEN_BOTTOM):y(SCREEN_BOTTOM+230)
					:fadebottom(0.1):fadeleft(0.1):diffusealpha(0):blend("add"):player(PLAYER_2)
				end;			
				ShowCommand=function(self)
					self:finishtweening():diffusealpha(1):linear(0.6):addy(-730):linear(0.1):diffusealpha(0) end;
				HideCommand=function(self)
					self:finishtweening():y(SCREEN_BOTTOM+250) end;
			};
		};
	};
};