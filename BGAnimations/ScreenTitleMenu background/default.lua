return Def.ActorFrame {

-- Def.ActorFrame {
	Def.Quad {
		InitCommand=function(self) self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):Center():diffuse(color("#96E7D7")):diffusebottomedge(color("#17FFD1")) end;
	};
	LoadActor("_cloud") .. {
		InitCommand=function(self) self:horizalign(left):xy(SCREEN_LEFT-20,SCREEN_CENTER_Y-200):zoom(0.45):diffusealpha(0.55) end;
		OnCommand=function(self) self:queuecommand("Transition") end;
		TransitionMessageCommand=function(self) self:linear(37):addx(550) end;
		OffCommand=function(self) self:stoptweening() end;
	};	
--};

--  Def.ActorFrame {
	LoadActor("_castle") .. {
		InitCommand=function(self) self:horizalign(left):xy(SCREEN_LEFT-20,SCREEN_CENTER_Y+90):zoom(0.5) end;
		OnCommand=function(self) self:queuecommand("Transition") end;
		TransitionMessageCommand=function(self) self:decelerate(3):y(SCREEN_CENTER_Y) end;
	};		
	LoadActor("_wall") .. {
		InitCommand=function(self) self:horizalign(left):xy(SCREEN_LEFT-20,SCREEN_CENTER_Y+95):zoom(0.5) end;
		OnCommand=function(self) self:queuecommand("Transition") end;
		TransitionMessageCommand=function(self) self:decelerate(3):y(SCREEN_CENTER_Y) end;
	};	
	LoadActor("_tavern") .. {
		InitCommand=function(self) self:horizalign(right):xy(SCREEN_RIGHT+16,SCREEN_CENTER_Y+100):zoom(0.75) end;
		OnCommand=function(self) self:queuecommand("Transition") end;
		TransitionMessageCommand=function(self) self:decelerate(3.2):y(SCREEN_CENTER_Y-70) end;
	};	
	Def.Quad {
		InitCommand=function(self) self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):Center():diffuse(color("#C9DF62")):diffusebottomedge(color("#7DF0DD")):diffusealpha(0.16) end;
	};
	LoadActor("_lightpost") .. {
		InitCommand=function(self) self:xy(SCREEN_CENTER_X+70,SCREEN_CENTER_Y+95):zoom(0.75) end;
		OnCommand=function(self) self:queuecommand("Transition") end;
		TransitionMessageCommand=function(self) self:decelerate(3):y(SCREEN_CENTER_Y) end;
	};
--};

};