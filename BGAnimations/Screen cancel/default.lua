return Def.ActorFrame {
	Def.Quad {
		OnCommand=function(self)
			self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):Center()
			self:diffuse(Color.Black):diffusealpha(0):draworder(12000)
			self:decelerate(0.35):diffusealpha(1)
		end;
	};
	LoadActor(THEME:GetPathS("_Screen","cancel")) .. {
		IsAction= true,
		StartTransitioningCommand=function(self) self:play() end;
	};
};
