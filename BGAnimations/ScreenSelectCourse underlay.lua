return Def.ActorFrame {
	
	Def.Quad {
		InitCommand=function(self)
			self:horizalign(center):vertalign(middle):zoomto(360,SCREEN_HEIGHT):x(SCREEN_CENTER_X-200):y(SCREEN_CENTER_Y)
			self:diffuse(color("#7F1414")):diffusealpha(0.95):fadeleft(0.05):faderight(0.05) 
		end;
		OffCommand=function(self)
			self:decelerate(0.3):diffusealpha(0)
		end;
	};
	
};