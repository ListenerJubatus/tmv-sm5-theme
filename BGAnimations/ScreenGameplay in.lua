return Def.ActorFrame {
	-- Doing it here helps with songs that forcibly hide the overlay and underlay 
	LoadActor(THEME:GetPathG("", "pause_menu"));
	Def.ActorFrame {
		LoadActor(THEME:GetPathG("_logo transition", "base")) .. {
					InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):diffuse(color("#000000")) end;
					OnCommand=function(self) self:diffusealpha(1):sleep(0.6):smooth(0.4):zoom(1.6):diffusealpha(0) end;
		};	
		LoadActor(THEME:GetPathG("_logo transition", "bottom parts")) .. {
					InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):diffuse(color("#000000")) end;
					OnCommand=function(self) self:diffusealpha(1):sleep(0.1):decelerate(0.1):diffusealpha(0) end;
		};	
		LoadActor(THEME:GetPathG("_logo transition", "middle parts")) .. {
					InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):diffuse(color("#000000")) end;
					OnCommand=function(self) self:diffusealpha(1):sleep(0.2):decelerate(0.1):diffusealpha(0) end;
		};	
		LoadActor(THEME:GetPathG("_logo transition", "side parts")) .. {
					InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):diffuse(color("#000000")) end;
					OnCommand=function(self) self:diffusealpha(1):sleep(0.3):decelerate(0.1):diffusealpha(0) end;
		};	
		LoadActor(THEME:GetPathG("_logo transition", "top parts")) .. {
					InitCommand=function(self) self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):diffuse(color("#000000")) end;
					OnCommand=function(self) self:diffusealpha(1):sleep(0.4):decelerate(0.1):diffusealpha(0) end;
		};
	};
};