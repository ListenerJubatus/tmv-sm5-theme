local t = Def.ActorFrame {};
	for ip, pn in ipairs(GAMESTATE:GetEnabledPlayers()) do
		local life_x_position = string.find(pn, "P1") and SCREEN_CENTER_X-45 or SCREEN_CENTER_X+45
		t[#t+1] = Def.ActorFrame {
		InitCommand=function(self) self:visible(GAMESTATE:IsHumanPlayer(pn)) end;
				Def.ActorFrame {
					InitCommand=function(self)
						self:x(Center1Player() and SCREEN_CENTER_X+150 or life_x_position):y(SCREEN_BOTTOM+40)
					end;
					OnCommand=function(self)
						self:sleep(1.1):decelerate(0.3):y(SCREEN_BOTTOM-20)
					end;
					OffCommand=function(self) self:sleep(0.6):decelerate(0.5):addy(73) end;
					LoadActor(THEME:GetPathG("_diff", "badge")) .. {
						 OnCommand=function(self) self:playcommand("Set") end;
						 ["CurrentSteps"..ToEnumShortString(pn).."ChangedMessageCommand"]=function(self) self:playcommand("Set") end;
						 SetCommand=function(self)
								local steps_data = GAMESTATE:GetCurrentSteps(pn)
								local song = GAMESTATE:GetCurrentSong();
									if song then
										if steps_data ~= nil then
										local st = steps_data:GetStepsType();
										local diff = steps_data:GetDifficulty();
										local cd = GetCustomDifficulty(st, diff);
										self:diffuse(CustomDifficultyToColor(cd));
									end
								end
						end;
					};
					LoadFont("ScreenGameplay diffmeter") .. { 
						InitCommand=function(self)
							self:y(5):zoom(0.4):horizalign(center):diffuse(color("#FFFFFF")):maxwidth(100) 
						end;
						OnCommand=function(self)
							self:diffusealpha(0.8):playcommand("Set")
						end;
						["CurrentSteps"..ToEnumShortString(pn).."ChangedMessageCommand"]=function(self) self:playcommand("Set") end;
						SetCommand=function(self)
								local steps_data = GAMESTATE:GetCurrentSteps(pn)
								local song = GAMESTATE:GetCurrentSong();
									if song then
										if steps_data ~= nil then
										local st = steps_data:GetStepsType();
										local diff = steps_data:GetDifficulty();
										local cd = GetCustomDifficulty(st, diff);
										self:settext(steps_data:GetMeter())
										self:diffusebottomedge(ColorLightTone(CustomDifficultyToColor(cd)));
										self:strokecolor(ColorDarkTone(CustomDifficultyToColor(cd)));
									end
								end
						end;
					};
				};
		};
	end;
	
return t