local t = Def.ActorFrame{};

if not GAMESTATE:IsCourseMode() then return t; end;


t[#t+1] = Def.ActorFrame { -- Color
		Def.Quad {
			InitCommand=function(self) self:zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y):diffuse(color("#712113")) end;
			StartCommand=function(self) self:diffusealpha(0):smooth(0.4):diffusealpha(1) end;
			FinishCommand=function(self) self:smooth(0.5):diffusealpha(0) end;
		};
	};


t[#t+1] = Def.Sprite {
	name="SongJacket";
	StartCommand=function(self) self:Center():diffusealpha(0):zoom(0.8):decelerate(0.4):diffusealpha(1):zoom(1) end;
	FinishCommand=function(self) self:smooth(0.5):diffusealpha(0) end;
	BeforeLoadingNextCourseSongMessageCommand=function(self) 
		local song = SCREENMAN:GetTopScreen():GetNextCourseSong(); 
		if song then
			if song:HasJacket() then
				self:Load(song:GetJacketPath()):scaletoclipped(256,256)
			elseif song:HasBanner() then
				self:Load(song:GetBannerPath()):scaletoclipped(256,80)
			else
				self:Load(THEME:GetPathG("Common fallback", "jacket")):scaletoclipped(256,256)
			end
		else
			self:diffusealpha(0)
		end
	end;
};

return t;