return Def.ActorFrame {
	Def.Quad {
		InitCommand=function(self) self:Center():zoomto(SCREEN_WIDTH,SCREEN_HEIGHT):diffuse(Color.Black) end;
		OnCommand=function(self) self:diffusealpha(0.5):smooth(0.8):diffusealpha(0.0) end;
	};	
	
	LoadActor(THEME:GetPathG("_door", "bottom")) .. {
		InitCommand=function(self) self:horizalign(center):vertalign(top):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y-8) end;
		OnCommand=function(self) self:smooth(0.8):y(SCREEN_BOTTOM):sleep(0.3):diffusealpha(0) end;
	};	
	
	LoadActor(THEME:GetPathG("_door", "top")) .. {
		InitCommand=function(self) self:horizalign(center):vertalign(bottom):x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y+100-10) end;
		OnCommand=function(self) self:smooth(0.8):y(SCREEN_TOP):sleep(0.3):diffusealpha(0) end;
	};

	LoadActor(THEME:GetPathS("ScreenTransition", "open door")) .. {
		StartTransitioningCommand=function(self)
			self:play();
		end;
	};	
};