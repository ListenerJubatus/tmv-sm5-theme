if not GAMESTATE:IsCourseMode() then
	return Def.ActorFrame {
		LoadActor(THEME:GetPathG("ScreenEvaluation", "StageDisplay")) .. {
			InitCommand=function(self)
				self:vertalign(top):x(SCREEN_RIGHT-180):y(SCREEN_TOP+16)
			end;
			OnCommand=function(self)
				self:addy(-73):sleep(0.9):decelerate(0.3):addy(73):bouncebegin(0.2):addy(-3):bounceend(0.3):addy(3)
			end;
			OffCommand=function(self)
				self:sleep(0.4):bouncebegin(0.5):addy(-73)
			end;
		}
	}
else
return Def.ActorFrame {	
	InitCommand=function(self) self:vertalign(top):x(SCREEN_RIGHT-180):y(SCREEN_TOP+16) end;
	OnCommand=function(self)
		self:addy(-73):sleep(0.9):decelerate(0.3):addy(73):bouncebegin(0.2):addy(-3):bounceend(0.3):addy(3)
	end;	
	OffCommand=function(self)
		self:sleep(0.4):bouncebegin(0.5):addy(-73)
	end;
	LoadActor( THEME:GetPathG("ScreenHeader", "menu flag") ) .. {
		BeginCommand=function(self)
			self:playcommand("Set")
		end;
		SetCommand=function(self)
			local curStage = GAMESTATE:GetCurrentStage();
			self:diffuse(color("#911B0B"));
		end
	};			
	LoadActor( THEME:GetPathG("ScreenHeader", "flag overlay") ) .. {
	};
	LoadFont("_decadentafrax 24px") .. {
		InitCommand=function(self)
			self:y(5):zoom(1):strokecolor(color("#000000")):diffuse(color("#FFFFFF")):horizalign(center):shadowlength(1):maxwidth(100)
		end;
		BeginCommand=function(self)
			self:playcommand("Set")
		end;
		CurrentSongChangedMessageCommand=function(self)
			self:playcommand("Set")
		end;
		SetCommand=function(self)
			local curStage = GAMESTATE:GetCurrentStage();
			local course = GAMESTATE:GetCurrentCourse()
			self:settext(string.upper(ToEnumShortString( course:GetCourseType() )))
			-- StepMania is being stupid so we have to do this here;
		end;
	};
};
end;