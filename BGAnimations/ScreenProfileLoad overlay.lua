local x = Def.ActorFrame{
	LoadFont("Common Normal")..{
		Text=ScreenString("Loading Profiles");
		InitCommand=function(self)
			self:horizalign(left):x(SCREEN_LEFT+80):y(SCREEN_BOTTOM-75):diffuse(color("#FFFFFF")):strokecolor(color("#000000")):shadowlength(1):zoom(0.75)
		end;
		OnCommand=function(self) 
			self:diffusealpha(0):linear(0.3):diffusealpha(1)
		end;
		OffCommand=function(self) 
			self:decelerate(0.15):diffusealpha(0)
		end;
	};
	Def.Sprite {
		Name="flagA",
		Texture=THEME:GetPathG("_load", "flag"),
		InitCommand=function(self)
			self:SetAllStateDelays(0.077):horizalign(left):x(SCREEN_LEFT+25):y(SCREEN_BOTTOM-75):diffuse(color("#FFFFFF")):shadowlength(1):zoom(0.75)
		end;
		OnCommand=function(self) 
			self:diffusealpha(0):linear(0.3):diffusealpha(1)
		end;
		OffCommand=function(self) 
			self:decelerate(0.15):diffusealpha(0)
		end;
	};	
};

x[#x+1] = Def.Actor {
	BeginCommand=function(self)
		if SCREENMAN:GetTopScreen():HaveProfileToLoad() then self:sleep(1); end;
		self:queuecommand("Load");
	end;
	LoadCommand=function() SCREENMAN:GetTopScreen():Continue(); end;
};

return x;