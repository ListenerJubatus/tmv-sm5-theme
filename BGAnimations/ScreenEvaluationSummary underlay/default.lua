local t = Def.ActorFrame {};
local st = GAMESTATE:GetCurrentStyle():GetStepsType();
local playedStages = STATSMAN:GetStagesPlayed();

for i=1,playedStages do
local playedStageStats = STATSMAN:GetPlayedStageStats(i);
-- Base
t[#t+1] = Def.ActorFrame {
	InitCommand=function(self)
		self:x(SCREEN_CENTER_X):y(SCREEN_CENTER_Y-170+(54*(i-1)))
	end;
	OnCommand=function(self)
		self:diffusealpha(0):zoomx(0.8):sleep(0.2*(i-1)):decelerate(0.3):diffusealpha(1):zoomx(1)
	end;
	OffCommand=function(self)
		self:sleep(0.2*(i-1)):decelerate(0.2):diffusealpha(0)
	end;
		Def.Sprite {
			InitCommand=function(self)
				self:y(25)
				local target = playedStageStats:GetPlayedSongs()[1];	
				if target and target:HasBanner() then
					self:Load(target:GetBannerPath())
					self:scaletoclipped(150,46)
				else
					self:visible(false)
				end;
			end;
		},
		Def.Sprite {
			Texture="_pod";
			InitCommand=function(self)
				self:vertalign(top):y(0)
			end;
		},
		Def.BitmapText {
			Font="Common Normal",
			InitCommand=function(self) self:vertalign(top):y(0+20):diffuse(color("#FFFFFF")):shadowlength(1):zoom(0.4):wrapwidthpixels(200/0.4) end;
			OnCommand=function(self) 
			local curSong = playedStageStats:GetPlayedSongs()[1];	
				if curSong:HasBanner() == false then
					self:settext(curSong:GetDisplayFullTitle())
					self:visible(true):addy(-5)
				else
					self:visible(false)
				end
			end;
		},			
	};
-- Score
for ip, pn in ipairs(GAMESTATE:GetHumanPlayers()) do
	local pStageStats = playedStageStats:GetPlayerStageStats( pn ); 
	local x_pos = string.find(pn, "P1") and SCREEN_CENTER_X-260 or SCREEN_CENTER_X+260;
	local grade_pos = string.find(pn, "P1") and SCREEN_CENTER_X-120 or SCREEN_CENTER_X+120;
	t[#t+1] = Def.BitmapText {
		Font="Common Normal",
		InitCommand=function(self)
		self:zoom(1):maxwidth(140):x(x_pos):y(SCREEN_CENTER_Y-170+22+(54*(i-1)))
		end;
		BeginCommand=function(self)
			self:diffuse(ColorLightTone(PlayerColor(pn)))
			:diffusebottomedge(ColorMidTone(PlayerColor(pn)))
			:strokecolor(ColorDarkTone(PlayerColor(pn)))
			:horizalign(center)
		end,
		OnCommand=function(self)
			if PREFSMAN:GetPreference("PercentageScoring") then
				self:settext(FormatPercentScore(pStageStats:GetPercentDancePoints()))
			else
				self:settext(pStageStats:GetScore())
			end;
			self:diffusealpha(0):sleep(0.4*(i-1)):decelerate(0.3):diffusealpha(1)
		end;
		OffCommand=function(self)
			self:sleep(0.2*(i-1)):decelerate(0.2):diffusealpha(0)
		end;
	};
	t[#t+1] = Def.Sprite{
		Texture=THEME:GetPathG("GradeDisplay", "Grade " .. playedStageStats:GetPlayerStageStats(pn):GetGrade());
		InitCommand=function(self)
			self:zoom(0.5):x(grade_pos):y(SCREEN_CENTER_Y-170+22+(54*(i-1)))
		end;
		OnCommand=function(self)
			self:diffusealpha(0):sleep(0.4*(i-1)):decelerate(0.3):diffusealpha(1)
		end;
		OffCommand=function(self)
			self:sleep(0.2*(i-1)):decelerate(0.2):diffusealpha(0)
		end;
	};
	t[#t+1] = Def.Sprite{
		InitCommand=function(self)
			self:zoom(0.5):x(grade_pos):y(SCREEN_CENTER_Y-170+22+(54*(i-1)))
		end;
		OnCommand=function(self)
			self:diffusealpha(0):sleep(0.4*(i-1)):decelerate(0.3):diffusealpha(1)
		end;
		OffCommand=function(self)
			self:sleep(0.2*(i-1)):decelerate(0.2):diffusealpha(0)
		end;
	};
	
end;

end;
collectgarbage();
return t;
